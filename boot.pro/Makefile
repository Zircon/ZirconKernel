################################################################################
## Copyright � 2011 Maxime Jeanson for the Zircon Project.                    ##
## This file is part of the Zircon Project.                                   ##
##                                                                            ##
## The Zircon Project is free software: you can redistribute it and/or modify ##
## it under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation, either version 3 of the License, or              ##
## (at your option) any later version.                                        ##
##                                                                            ##
## The Zircon Project is distributed in the hope that it will be useful, but  ##
## WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ##
## or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ##
## for more details.                                                          ##
##                                                                            ##
## You should have received a copy of the GNU General Public License along    ##
## with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ##
################################################################################

PRONAME=	$(basename $(notdir $(abspath ./  )))
PRODIR=		$(notdir $(abspath ./  ))
PROEXEC=	$(PRONAME)

INCDIR=		include
ODIR=		../build

SRC:=		$(shell find -type f -name \*.asm)
SRCAS:=		$(shell find -type f -name \*.s)
SRCCC:=		$(shell find -type f -name \*.c)
SRCCXX:=	$(shell find -type f -name \*.cpp)

OOBJ=		$(ODIR)/$(notdir $@)
OBJ:=		$(patsubst %.asm,%.o,$(SRC)) $(patsubst %.c,%.o,$(SRCCC)) $(patsubst %.cpp,%.o,$(SRCCXX)) $(patsubst %.s,%.o,$(SRCAS))

OPP:=		$(ODIR)/$(notdir $@)
PP:=		$(patsubst %.asm,%.pasm,$(SRC)) $(patsubst %.c,%.s,$(SRCCC)) $(patsubst %.cpp,%.s,$(SRCCXX)) $(patsubst %.s,%.o,$(SRCAS))

ALLFILES:=	$(shell find -type f -name \*.inc) $(SRC)

ASFLAGS=	-s "$(ODIR)/error/$(notdir $*).txt" -f $(OFORMAT) -I $(INCDIR)/ -P "$*.casm" -P "$(INCDIR)/nasm/nasm.inc" -o "$(OOBJ)" $^
CCFLAGS=	-m32 -masm=intel -std=c99 --no-builtin -Wa,--keep-locals -fno-stack-protector -nostartfiles -c -iquote $(INCDIR)/ -I ../include/ -o $(OOBJ) $^
CXXFLAGS=	-m32 -masm=intel -c -fno-exceptions -nostdlib -nodefaultlibs --no-builtin -Wa,--keep-locals -fno-stack-protector -nostartfiles -iquote $(INCDIR)/ -I ../include/ -o $(OOBJ) $^

CCFLAGSAS=	-m32 -masm=intel -std=c99 --no-builtin -Wa,--keep-locals -fno-stack-protector -nostartfiles -S -iquote $(INCDIR)/ -I ../include/ -o $(OOBJ) $^
CXXFLAGSAS=	-m32 -masm=intel -S -fno-exceptions --no-builtin -Wa,--keep-locals -fno-stack-protector -nostartfiles -iquote $(INCDIR)/ -I ../include/ -o $(OOBJ) $^

AS=			@(nasm -s -f $(OFORMAT) -I $(INCDIR)/ -P "$*.casm" -P "$(INCDIR)/nasm/nasm.inc" -o "$(OOBJ)" $^ >> $(ODIR)/error/$(PRODIR).log)
CC=			@/usr/local/cross/bin/x86_64-elf-gcc
CXX=		@/usr/local/cross/bin/x86_64-elf-g++
PAS=		@(nasm -Z "$(ODIR)/error/$(PRODIR).log" -E -I $(INCDIR)/ -P "$*.casm" -P "$(INCDIR)/nasm/nasm.inc" -o "$(OOBJ)" $^)
OFORMAT=	elf32

LINK=		@(make -C $(ODIR))
INSTALL=	@(sh scripts/install.sh)
BACKUP=		@(sh scripts/backup.sh)
CLEAN=		@(rm $(ODIR)/*.o | rm $(ODIR)/*.pasm)
RUN=		@(qemu '/media/OS/vdisk/zircon.vhd')

all: $(OBJ)
	@echo

run: all
	$(RUN)

%.pasm: %.asm
	@(echo "    PAS: ($^) -> ($(OOBJ))")
	-@(for file in $^; do fgrep -i --no-filename -e proc -e idata -e istruc $$file; done; true) > $*.cpasm
	@sed -e '/^\s*proc\s*[A-Za-z0-9_.]*/!d' -e 's/^\s*proc\s*\([A-Za-z0-9_.]*\)\(.*\)/%define __PROC_\1/ig' -e '/%define __PROC_\w/!d' $*.cpasm > $*.casm
	@sed -e '/^\s*idata\s*[A-Za-z0-9_.]*/!d' -e 's/^\s*idata\s*\([A-Za-z0-9_.]*\)\(.*\)/%define __IDATA_\1/ig' -e '/%define __IDATA_\w/!d' $*.cpasm >> $*.casm
	@sed -e '/^\s*istruc\s*[A-Za-z0-9_.]*/!d' -e 's/^\s*istruc\s*\([A-Za-z0-9_.]*\)\(.*\)/%define __ISTRUC_\1/ig' -e '/%define __ISTRUC_\w/!d' $*.cpasm >> $*.casm
	$(PAS)

	@(rm $*.cpasm | rm $*.casm)

%.s: %.c
	@(echo "    PCC:  ($^) -> ($(OOBJ))")
	$(CC)  $(CCFLAGSAS)

%.s: %.cpp
	@(echo "    PCXX: ($^) -> ($(OOBJ))")
	$(CXX) $(CXXFLAGSAS)

%.o: %.asm
	@(echo "    AS:  ($^) -> ($(OOBJ))")
	-@(for file in $^; do fgrep -i --no-filename -e proc -e idata -e istruc $$file; done; true) > $*.cpasm
	@sed -e '/^\s*proc\s*[A-Za-z0-9_.]*/!d' -e 's/^\s*proc\s*\([A-Za-z0-9_.]*\)\(.*\)/%define __PROC_\1/ig' -e '/%define __PROC_\w/!d' $*.cpasm > $*.casm
	@sed -e '/^\s*idata\s*[A-Za-z0-9_.]*/!d' -e 's/^\s*idata\s*\([A-Za-z0-9_.]*\)\(.*\)/%define __IDATA_\1/ig' -e '/%define __IDATA_\w/!d' $*.cpasm >> $*.casm
	@sed -e '/^\s*istruc\s*[A-Za-z0-9_.]*/!d' -e 's/^\s*istruc\s*\([A-Za-z0-9_.]*\)\(.*\)/%define __ISTRUC_\1/ig' -e '/%define __ISTRUC_\w/!d' $*.cpasm >> $*.casm
	$(AS)
	@(rm $*.cpasm | rm $*.casm)

%.o: %.c
	@(echo "    CC:  ($^) -> ($(OOBJ))")
	$(CC)  $(CCFLAGS)

%.o: %.cpp
	@(echo "    CXX: ($^) -> ($(OOBJ))")
	$(CXX) $(CXXFLAGS)

getTodo:
	-@for file in $(ALLFILES:Makefile=); do fgrep --color="ALWAYS" -H -e TODO -e FIXME $$file; done; true

pre: $(PP)
	@echo "    DONE!"

clean:
	$(CLEAN)
