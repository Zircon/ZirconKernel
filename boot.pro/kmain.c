unsigned int pciConfigReadWord (unsigned short bus, unsigned short slot,
                                  unsigned short func, unsigned short offset);

unsigned int pciConfigReadDevice (unsigned int dev, unsigned short func,
                                  unsigned short offset);

#define asm __asm__

/* desactive les interruptions */
#define cli asm("cli"::)

/* reactive les interruptions */
#define sti asm("sti"::)

/* ecrit un octet sur un port */
#define outb(port,value) \
        asm volatile ("out dx, al" :: "d" (port), "a" (value));

/* ecrit un octet sur un port */
#define outw(port,value) \
        asm volatile ("out dx, ax" :: "a" (value), "d" (port));

/* ecrit un octet sur un port */
#define outd(port,value) \
        asm volatile ("out dx, eax" :: "a" (value), "d" (port));

/* ecrit un octet sur un port et marque une temporisation  */
#define outbp(port,value) \
        asm volatile ("outb %%al, %%dx; jmp 1f; 1:" :: "d" (port), "a" (value));

/* lit un octet sur un port */
#define inb(port) ({    \
        unsigned char _v;       \
        asm volatile ("in al, dx" : "=a" (_v) : "d" (port)); \
        _v;     \
})

/* lit un octet sur un port */
#define inw(port) ({    \
        unsigned short _v;       \
        asm volatile ("in ax, dx" : "=a" (_v) : "d" (port)); \
        _v;     \
})

/* lit un octet sur un port */
#define ind(port) ({    \
        unsigned int _v;       \
        asm volatile ("in eax, dx" : "=a" (_v) : "d" (port)); \
        _v;     \
})

void _kmain(unsigned int key, void* mbi)
{
    unsigned char *vram = (void*)0xe0000000;

    for(int i=0; i<10; i++)
    {
        vram[i] = 'a';
    }

    int dev = 0;
    for(int i = 0; i<0x100; i+=8)
    {
        if(((pciConfigReadDevice (dev, 0, 8)) & 0xffff) == 0x0501)
        {
            ((unsigned int*)vram)[i] = pciConfigReadDevice (dev, 0, 0x10);
            ((unsigned int*)vram)[i+1] = pciConfigReadDevice (dev, 0, 0x14);
            ((unsigned int*)vram)[i+2] = pciConfigReadDevice (dev, 0, 0x18);
            ((unsigned int*)vram)[i+3] = pciConfigReadDevice (dev, 0, 0x1c);
            ((unsigned int*)vram)[i+4] = pciConfigReadDevice (dev, 0, 0x20);
            ((unsigned int*)vram)[i+5] = pciConfigReadDevice (dev, 0, 0x24);
            ((unsigned int*)vram)[i+6] = pciConfigReadDevice (dev, 0, 0x0);
            ((unsigned int*)vram)[i+7] = pciConfigReadDevice (dev, 0, 0x8);
        }

        dev++;
    }
    while(1);
}



unsigned int pciConfigReadWord (unsigned short bus, unsigned short slot,
                                  unsigned short func, unsigned short offset)
{
   unsigned int address;
   unsigned int lbus = (unsigned int)bus;
   unsigned int lslot = (unsigned int)slot;
   unsigned int lfunc = (unsigned int)func;
   unsigned int tmp = 0;

   /* create configuration address as per Figure 1 */
   address = (unsigned int)((lbus << 16) | (lslot << 11) |
             (lfunc << 8) | (offset & 0xfc) | ((unsigned int)0x80000000));

   /* write out the address */
   outd (0xCF8, address);
   /* read in the data */
   /* (offset & 2) * 8) = 0 will choose the fisrt word of the 32 bits register */
   tmp = (unsigned int)((ind (0xCFC)));
   return (tmp);
}

unsigned int pciConfigReadDevice (unsigned int dev, unsigned short func,
                                  unsigned short offset)
{
   unsigned int address;
   unsigned int ldev = (unsigned int)dev;
   unsigned int lfunc = (unsigned int)func;
   unsigned int tmp = 0;

   /* create configuration address as per Figure 1 */
   address = (unsigned int)((ldev << 11) |
             (lfunc << 8) | (offset & 0xfc) | ((unsigned int)0x80000000));

   /* write out the address */
   outd (0xCF8, address);
   /* read in the data */
   /* (offset & 2) * 8) = 0 will choose the fisrt word of the 32 bits register */
   tmp = (unsigned int)((ind (0xCFC)));
   return (tmp);
}
