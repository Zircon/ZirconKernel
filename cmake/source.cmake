include(FetchContent)
include(ExternalProject)
include(CMakePackageConfigHelpers)
include(GenerateExportHeader)
include(GNUInstallDirs)
include(CTest)

function(string_starts_with str search)
    string(FIND "${str}" "${search}" out)
    if("${out}" EQUAL 0)
        return(true)
    endif()
    return(false)
endfunction()

macro(_create_target)
    set(oneValueArgs NAME TYPE NAMESPACE HEADER_NAMESPACE GROUP COMPONENT TARGET)
    set(multiValueArgs
            PUBLIC_HEADERS PRIVATE_HEADERS
            PUBLIC_GENERATED_HEADERS PRIVATE_GENERATED_HEADERS
            PUBLIC_PRECOMPILED_HEADERS PRIVATE_PRECOMPILED_HEADERS
            PUBLIC_SOURCES PRIVATE_SOURCES
            PUBLIC_GENERATED_SOURCES PRIVATE_GENERATED_SOURCES
            PUBLIC_DEPENDENCIES PRIVATE_DEPENDENCIES
            PUBLIC_INCLUDE_DIRECTORIES PRIVATE_INCLUDE_DIRECTORIES
            PUBLIC_COMPILE_DEFINITIONS PRIVATE_COMPILE_DEFINITIONS
            PUBLIC_COMPILE_OPTIONS PRIVATE_COMPILE_OPTIONS
            PUBLIC_LINK_OPTIONS PRIVATE_LINK_OPTIONS)

    cmake_parse_arguments(ARG "${options}" "${oneValueArgs}"
            "${multiValueArgs}" ${ARGN})

    string(REPLACE "::" "-" ARG_TARGET_OUTPUT_NAME "${ARG_NAME}")
    string(REPLACE "${ARG_NAMESPACE}" "" ARG_TARGET_ALIAS "${ARG_NAME}")
    string(REPLACE "::" "-" ARG_TARGET_NAME "${ARG_TARGET_ALIAS}")
    string(REPLACE "::" "_" ARG_TARGET_NAME_SNEAK "${ARG_NAME}")
    string(TOUPPER "${ARG_TARGET_NAME_SNEAK}" ARG_TARGET_NAME_SNEAK)
    string(TOUPPER "${ARG_TYPE}" ARG_TYPE)

    if ("${ARG_TYPE}" STREQUAL "LIBRARY")
        add_library("${ARG_TARGET_NAME}")
        if (NOT "${ARG_TARGET_NAME}" STREQUAL "${ARG_NAME}")
            add_library("${ARG_NAME}" ALIAS "${ARG_TARGET_NAME}")
        endif()
    elseif("${ARG_TYPE}" STREQUAL "INTERFACE_LIBRARY")
        add_library("${ARG_TARGET_NAME}" INTERFACE)
        if (NOT "${ARG_TARGET_NAME}" STREQUAL "${ARG_NAME}")
            add_library("${ARG_NAME}" ALIAS "${ARG_TARGET_NAME}")
        endif()
    elseif("${ARG_TYPE}" STREQUAL "EXECUTABLE")
        add_executable("${ARG_TARGET_NAME}")
    else()
        message(FATAL_ERROR "Could not create target of type ${ARG_TYPE}")
    endif()

    set_property(TARGET "${ARG_TARGET_NAME}" PROPERTY CXX_STANDARD 20)
    set_property(TARGET "${ARG_TARGET_NAME}" PROPERTY TARGET_NAME "${ARG_TARGET_NAME}")
    set_property(TARGET "${ARG_TARGET_NAME}" PROPERTY TARGET_NAME_SNEAK "${ARG_TARGET_NAME_SNEAK}")
    set_property(TARGET "${ARG_TARGET_NAME}" PROPERTY HEADER_NAMESPACE "${ARG_HEADER_NAMESPACE}")
    set_property(TARGET "${ARG_TARGET_NAME}" PROPERTY "OUTPUT_NAME" "${ARG_TARGET_OUTPUT_NAME}")

    if ("${ARG_TYPE}" STREQUAL "LIBRARY")
        generate_export_header("${ARG_TARGET_NAME}"
                EXPORT_MACRO_NAME "${ARG_TARGET_NAME_SNEAK}_API"
                EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/include/${ARG_HEADER_NAMESPACE}/common.h)

        list(APPEND ARG_PUBLIC_GENERATED_HEADERS
                "include/${ARG_HEADER_NAMESPACE}/common.h")

    endif()

    if(NOT "${ARG_TYPE}" STREQUAL "INTERFACE_LIBRARY")
        target_include_directories("${ARG_TARGET_NAME}"
            PUBLIC
                $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
                $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/include>
                $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>

            PRIVATE
                $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src>)
    endif()


    foreach (SCP IN ITEMS "PUBLIC" "PRIVATE")
        if("${ARG_TYPE}" STREQUAL "INTERFACE_LIBRARY")
            set(_SCP "INTERFACE")
        else()
            set(_SCP "${SCP}")
        endif()

        foreach (ITEM ${ARG_${SCP}_PRECOMPILED_HEADERS})
            string(REGEX REPLACE "^include/(.*)$" "\\1" ITEM "${ITEM}")

            target_precompile_headers("${ARG_TARGET_NAME}"
                ${_SCP}
                    $<BUILD_INTERFACE:${ITEM}>
                    $<INSTALL_INTERFACE:<${ITEM}$<ANGLE-R>>)
        endforeach ()

        foreach (SCP_TYPE IN ITEMS "SOURCES" "HEADERS")
            foreach (ITEM ${ARG_${SCP}_${SCP_TYPE}})
                if ("${SCP_TYPE}" STREQUAL "HEADERS" AND "${SCP}" STREQUAL "PUBLIC")
                    get_target_property(ITEMS "${ARG_TARGET_NAME}" PUBLIC_HEADER)
                    if("${ITEMS}" STREQUAL "ITEMS-NOTFOUND")
                        set_target_properties("${ARG_TARGET_NAME}" PROPERTIES PUBLIC_HEADER "${ITEM}")
                    else()
                        set_target_properties("${ARG_TARGET_NAME}" PROPERTIES PUBLIC_HEADER "${ITEMS};${ITEM}")
                    endif()
                endif()

                target_sources("${ARG_TARGET_NAME}"
                        ${_SCP} $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/${ITEM}>
                        ${_SCP} $<INSTALL_INTERFACE:${ITEM}>)
            endforeach ()

            foreach (ITEM ${ARG_${SCP}_GENERATED_${SCP_TYPE}})
                target_sources("${ARG_TARGET_NAME}"
                        ${_SCP} $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/${ITEM}>
                        ${_SCP} $<INSTALL_INTERFACE:${ITEM}>)
            endforeach ()
        endforeach ()

        foreach (ITEM ${ARG_${SCP}_DEPENDENCIES})
            target_link_libraries("${ARG_TARGET_NAME}"
                    ${_SCP} ${ITEM})
        endforeach ()

        foreach (ITEM ${ARG_${SCP}_INCLUDE_DIRECTORIES})
            target_include_directories("${ARG_TARGET_NAME}" ${_SCP} ${ITEM})
        endforeach ()

        foreach (ITEM ${ARG_${SCP}_COMPILE_DEFINITIONS})
            target_compile_definitions("${ARG_TARGET_NAME}" ${_SCP} ${ITEM})
        endforeach ()

        foreach (ITEM ${ARG_${SCP}_COMPILE_OPTIONS})
            target_compile_options("${ARG_TARGET_NAME}" ${_SCP} ${ITEM})
        endforeach ()

        foreach (ITEM ${ARG_${SCP}_LINK_OPTIONS})
            target_link_options("${ARG_TARGET_NAME}" ${_SCP} ${ITEM})
        endforeach ()
    endforeach ()

    if (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/${ARG_TARGET_NAME}.ld")
        target_link_options("${ARG_TARGET_NAME}" PRIVATE -T "${CMAKE_CURRENT_SOURCE_DIR}/${ARG_TARGET_NAME}.ld")
    else()
        message(STATUS "Could not find '${CMAKE_CURRENT_SOURCE_DIR}/${ARG_TARGET_NAME}.ld'")
    endif ()

    configure_package_config_file("${CMAKE_CURRENT_SOURCE_DIR}/config.cmake.in"
            "${CMAKE_CURRENT_BINARY_DIR}/${ARG_TARGET_NAME}-config.cmake"
            INSTALL_DESTINATION "lib/cmake/${ARG_TARGET_NAME}"
            NO_SET_AND_CHECK_MACRO
            NO_CHECK_REQUIRED_COMPONENTS_MACRO)

    write_basic_package_version_file(
            "${CMAKE_CURRENT_BINARY_DIR}/${ARG_TARGET_NAME}-config-version.cmake"
            VERSION "1.0.0"
            COMPATIBILITY AnyNewerVersion)
endmacro(_create_target)

macro(create_library)
    set(oneValueArgs NAME NAMESPACE HEADER_NAMESPACE COMPONENT TARGET)
    set(multiValueArgs
            PUBLIC_HEADERS PRIVATE_HEADERS
            PUBLIC_GENERATED_HEADERS PRIVATE_GENERATED_HEADERS
            PUBLIC_SOURCES PRIVATE_SOURCES
            PUBLIC_GENERATED_SOURCES PRIVATE_GENERATED_SOURCES
            PUBLIC_DEPENDENCIES PRIVATE_DEPENDENCIES
            PUBLIC_INCLUDE_DIRECTORIES PRIVATE_INCLUDE_DIRECTORIES
            PUBLIC_COMPILE_DEFINITIONS PRIVATE_COMPILE_DEFINITIONS
            PUBLIC_COMPILE_OPTIONS PRIVATE_COMPILE_OPTIONS)

    cmake_parse_arguments(ARG "${options}" "${oneValueArgs}"
            "${multiValueArgs}" ${ARGN})

    option(BUILD_SHARED_LIBS "Build shared library" ON)

    create_precompiled_headers_library(
        NAME "${ARG_NAME}-PCH"
        NAMESPACE ${ARG_NAMESPACE}
        HEADER_NAMESPACE ${ARG_HEADER_NAMESPACE}

        PRIVATE_HEADERS
            ${ARG_PUBLIC_HEADERS}
            ${ARG_PRIVATE_HEADERS}
            ${ARG_PUBLIC_GENERATED_HEADERS}
            ${ARG_PRIVATE_GENERATED_HEADERS}
    )

    _create_target(
            TYPE library
            NAME ${ARG_NAME}
            NAMESPACE ${ARG_NAMESPACE}
            COMPONENT ${ARG_COMPONENT}
            TARGET ${ARG_TARGET}

            HEADER_NAMESPACE ${ARG_HEADER_NAMESPACE}
            PUBLIC_HEADERS ${ARG_PUBLIC_HEADERS}
            PRIVATE_HEADERS ${ARG_PRIVATE_HEADERS}

            PUBLIC_PRECOMPILED_HEADERS
                ${ARG_PUBLIC_HEADERS}
                ${ARG_PUBLIC_GENERATED_HEADERS}

            PUBLIC_GENERATED_HEADERS ${ARG_PUBLIC_GENERATED_HEADERS}
            PRIVATE_GENERATED_HEADERS ${ARG_PRIVATE_GENERATED_HEADERS}
            PUBLIC_SOURCES ${ARG_PUBLIC_SOURCES}
            PRIVATE_SOURCES ${ARG_PRIVATE_SOURCES}
            PUBLIC_GENERATED_SOURCES ${ARG_PUBLIC_GENERATED_SOURCES}
            PRIVATE_GENERATED_SOURCES ${ARG_PRIVATE_GENERATED_SOURCES}
            PUBLIC_DEPENDENCIES ${ARG_PUBLIC_DEPENDENCIES}
            PRIVATE_DEPENDENCIES ${ARG_PRIVATE_DEPENDENCIES}
            PUBLIC_INCLUDE_DIRECTORIES ${ARG_PUBLIC_INCLUDE_DIRECTORIES}
            PRIVATE_INCLUDE_DIRECTORIES ${ARG_PRIVATE_INCLUDE_DIRECTORIES}
            PUBLIC_COMPILE_DEFINITIONS ${ARG_PUBLIC_COMPILE_DEFINITIONS}
            PRIVATE_COMPILE_DEFINITIONS ${ARG_PRIVATE_COMPILE_DEFINITIONS}
            PUBLIC_COMPILE_OPTIONS ${ARG_PUBLIC_COMPILE_OPTIONS}
            PRIVATE_COMPILE_OPTIONS ${ARG_PRIVATE_COMPILE_OPTIONS}
    )

    get_property(ARG_TARGET_NAME TARGET "${ARG_NAME}" PROPERTY "TARGET_NAME")
    set_property(TARGET "${ARG_TARGET_NAME}" PROPERTY "ADDITIONAL_TARGETS" "${ARG_TARGET_NAME}-PCH")

    install_library(TARGET "${ARG_NAME}" NAMESPACE "${ARG_NAMESPACE}")
endmacro(create_library)

function(create_precompiled_headers_library)
    set(oneValueArgs NAME NAMESPACE HEADER_NAMESPACE)
    set(multiValueArgs
            PUBLIC_HEADERS PRIVATE_HEADERS
            PUBLIC_DEPENDENCIES PRIVATE_DEPENDENCIES
            PUBLIC_COMPILE_DEFINITIONS PRIVATE_COMPILE_DEFINITIONS
            PUBLIC_COMPILE_OPTIONS PRIVATE_COMPILE_OPTIONS)

    cmake_parse_arguments(ARG "${options}" "${oneValueArgs}"
            "${multiValueArgs}" ${ARGN})

    option(BUILD_SHARED_LIBS "Build shared library" ON)

    _create_target(
            TYPE INTERFACE_LIBRARY
            NAME ${ARG_NAME}
            NAMESPACE ${ARG_NAMESPACE}
            HEADER_NAMESPACE ${ARG_HEADER_NAMESPACE}
            PUBLIC_PRECOMPILED_HEADERS ${ARG_PUBLIC_HEADERS}
            PRIVATE_PRECOMPILED_HEADERS ${ARG_PRIVATE_HEADERS}
            PUBLIC_DEPENDENCIES ${ARG_PUBLIC_DEPENDENCIES}
            PRIVATE_DEPENDENCIES ${ARG_PRIVATE_DEPENDENCIES}
            PUBLIC_COMPILE_DEFINITIONS ${ARG_PUBLIC_COMPILE_DEFINITIONS}
            PRIVATE_COMPILE_DEFINITIONS ${ARG_PRIVATE_COMPILE_DEFINITIONS}
            PUBLIC_COMPILE_OPTIONS ${ARG_PUBLIC_COMPILE_OPTIONS}
            PRIVATE_COMPILE_OPTIONS ${ARG_PRIVATE_COMPILE_OPTIONS}
    )
endfunction(create_precompiled_headers_library)

macro(create_executable)
    set(oneValueArgs NAME)
    set(multiValueArgs
            HEADERS
            GENERATED_HEADERS
            SOURCES
            GENERATED_SOURCES
            DEPENDENCIES
            INCLUDE_DIRECTORIES
            COMPILE_DEFINITIONS
            COMPILE_OPTIONS
            LINK_OPTIONS)

    cmake_parse_arguments(ARG "${options}" "${oneValueArgs}"
            "${multiValueArgs}" ${ARGN})

    _create_target(
            TYPE executable
            NAME ${ARG_NAME}
            PRIVATE_HEADERS ${ARG_HEADERS}
            PRIVATE_GENERATED_HEADERS ${ARG_GENERATED_HEADERS}
            PRIVATE_SOURCES ${ARG_SOURCES}
            PRIVATE_GENERATED_SOURCES ${ARG_GENERATED_SOURCES}
            PRIVATE_DEPENDENCIES ${ARG_DEPENDENCIES}
            PRIVATE_INCLUDE_DIRECTORIES ${ARG_INCLUDE_DIRECTORIES}
            PRIVATE_COMPILE_DEFINITIONS ${ARG_COMPILE_DEFINITIONS}
            PRIVATE_COMPILE_OPTIONS ${ARG_COMPILE_OPTIONS}
            PRIVATE_LINK_OPTIONS ${ARG_LINK_OPTIONS}
    )
    install_library(TARGET "${ARG_NAME}" NAMESPACE "${ARG_NAMESPACE}")
endmacro(create_executable)

macro(install_library)
    set(oneValueArgs TARGET NAMESPACE)
    set(multiValueArgs "")

    cmake_parse_arguments(INSTALL_LIBRARY "${options}" "${oneValueArgs}"
            "${multiValueArgs}" ${ARGN})

    get_property(INSTALL_LIBRARY_TARGET_NAME
            TARGET "${INSTALL_LIBRARY_TARGET}"
            PROPERTY "TARGET_NAME")
    get_property(INSTALL_LIBRARY_OUTPUT_NAME
            TARGET "${INSTALL_LIBRARY_TARGET}"
            PROPERTY "OUTPUT_NAME")
    get_property(INSTALL_LIBRARY_INSTALL_HEADER_PATH
            TARGET "${INSTALL_LIBRARY_TARGET}"
            PROPERTY "HEADER_NAMESPACE")
    get_property(ARG_ALL_TARGETS
            TARGET "${INSTALL_LIBRARY_TARGET}"
            PROPERTY "ADDITIONAL_TARGETS")

    set(ARG_ALL_TARGETS ${INSTALL_LIBRARY_TARGET_NAME} ${ARG_ALL_TARGETS})

    option(BUILD_SHARED_LIBS "Build shared library" ON)

    install(TARGETS ${ARG_ALL_TARGETS}
            EXPORT "${INSTALL_LIBRARY_OUTPUT_NAME}-targets"
            ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
            LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
            RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
            INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})

    install(EXPORT "${INSTALL_LIBRARY_OUTPUT_NAME}-targets"
            FILE "${INSTALL_LIBRARY_OUTPUT_NAME}-targets.cmake"
            NAMESPACE ${INSTALL_LIBRARY_NAMESPACE}
            DESTINATION "lib/cmake/${INSTALL_LIBRARY_OUTPUT_NAME}")

    install(DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/include/"
            DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}")

    install(DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/include/"
            DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}")

    install(FILES
            "${CMAKE_CURRENT_BINARY_DIR}/${INSTALL_LIBRARY_OUTPUT_NAME}-config.cmake"
            DESTINATION "lib/cmake/${INSTALL_LIBRARY_OUTPUT_NAME}")

    export(EXPORT "${INSTALL_LIBRARY_OUTPUT_NAME}-targets"
           NAMESPACE "${INSTALL_LIBRARY_NAMESPACE}"
           FILE "${CMAKE_CURRENT_BINARY_DIR}/${INSTALL_LIBRARY_OUTPUT_NAME}-targets.cmake")
endmacro(install_library)

macro(install_content name)
    string(REPLACE ";" "\ " ARGS "${ARGV}")
    string(REPLACE "${name} " "" ARGS "${ARGS}")
    string(TOLOWER ${name} contentNameLower)

    configure_file(${CMAKE_SOURCE_DIR}/cmake/include_content.cmake.in ${FETCHCONTENT_BASE_DIR}/${contentNameLower}-subbuild/CMakeLists.txt @ONLY)

    if ("${FETCHCONTENT_BASE_DIR}/${contentNameLower}-subbuild/CMakeLists.txt" IS_NEWER_THAN "${FETCHCONTENT_BASE_DIR}/${contentNameLower}-stamp/${contentNameLower}-populate-configure")
        execute_process(
                COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" "."
                RESULT_VARIABLE result
                ${outputOptions}
                WORKING_DIRECTORY "${FETCHCONTENT_BASE_DIR}/${contentNameLower}-subbuild"
        )
        if (result)
            if (capturedOutput)
                message("${capturedOutput}")
            endif ()
            message(FATAL_ERROR "Build step for ${contentName} failed: ${result}")
        endif ()
        execute_process(
                COMMAND ${CMAKE_COMMAND} --build . -j 15
                RESULT_VARIABLE result
                ${outputOptions}
                WORKING_DIRECTORY "${FETCHCONTENT_BASE_DIR}/${contentNameLower}-subbuild"
        )
        if (result)
            if (capturedOutput)
                message("${capturedOutput}")
            endif ()
            message(FATAL_ERROR "Build step for ${contentName} failed: ${result}")
        endif ()
    endif()

    list(APPEND CMAKE_PREFIX_PATH "${FETCHCONTENT_BASE_DIR}/${contentNameLower}-install/")
    list(APPEND CMAKE_MODULE_PATH "${FETCHCONTENT_BASE_DIR}/${contentNameLower}-install/")
endmacro(install_content)
