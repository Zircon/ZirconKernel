;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright © 2011-2012 Maxime Jeanson for the Zircon Project.               ;;
;; This file is part of the Zircon Project.                                   ;;
;;                                                                            ;;
;; The Zircon Project is free software: you can redistribute it and/or modify ;;
;; it under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or              ;;
;; (at your option) any later version.                                        ;;
;;                                                                            ;;
;; The Zircon Project is distributed in the hope that it will be useful, but  ;;
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ;;
;; or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ;;
;; for more details.                                                          ;;
;;                                                                            ;;
;; You should have received a copy of the GNU General Public License along    ;;
;; with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

%ifndef MULTIBOOT.INC
%define MULTIBOOT.INC

;;;;;;;;;;;;;;;;;;;;;;;
;; MULTIBOOT V0.6.96 ;;
;;;;;;;;;;;;;;;;;;;;;;;

; This specification can be found at
; http://www.gnu.org/software/grub/manual/multiboot/html_node/index.html

; The loaded OS must have the control on that stat:
; EAX	= 0x2badb002
; EBX	= multibootInfo pointer
; DS	= 32 bit rw data segment starting at 0, ending at 0xffffffff, same for:
;		  ES, FS, GS, SS
; A20 gate enable
; CR0	= (PG): clear; (PE): set
; EFLAGS= (VM): clear; (IF): clear
; Everuthing else undefined

;-----------------------;
; MULTIBOOT HEADER DEFS ;
;-----------------------;
%define MBH_MAGIC			0x1badb002

%define MBH_CHECKSUM(flag)	-(0x1badb002+flag)

%define MBH_FLAGS_PAGING	0x00000001	; Set to indicate that the modules must
										; be aligned on page (4Kb) boundary.
%define MBH_FLAGS_MEMORY	0x00000002	; Set if the OS need at least the mem_*
										; feild to be fill.
%define MBH_FLAGS_VIDEO		0x00000004	; Set to indicate that video setting
										; feilds are provided by the OS.
%define MBH_FLAGS_LOADING	0x00010000	; Set to indicate that loading pointer
										; feilds are provided by the OS.

%define MBH_MODE_GRAPHIC	0x00000000
%define MBH_MODE_TEXT		0x00000001

;---------------------;
; MULTIBOOT INFO DEFS ;
;---------------------;
%define MBI_FLAGS_MEM		0x00000001
%define MBI_FLAGS_BOOT_DEV	0x00000002
%define MBI_FLAGS_CMDLINE	0x00000004
%define MBI_FLAGS_MODS		0x00000008
%define MBI_FLAGS_SYMS		0x00000010
%define MBI_FLAGS_MMAP		0x00000020
%define MBI_FLAGS_DRIVES	0x00000040
%define MBI_FLAGS_CFG_TABLE	0x00000080
%define MBI_FLAGS_BL_NAME	0x00000100
%define MBI_FLAGS_APM_TABLE	0x00000200
%define MBI_FLAGS_VBE		0x00000400

;------------------;
; MULTIBOOT HEADER ;
;------------------;
struc multibootHeader
	; These feilds are required.
	.magic:			resb 4	; For multiboot v0.6.96, this feild must be
							; 0x1badb002.

	.flags:			resb 4	; Flags feild, see the coresponding defs for info.

	.checksum:		resb 4	; Checksum computed as: -(magic + flags).

	; These feilds are valid if bit 16 of the flags field is set. Otherwise this
	; block should be ignore by the loader.
	.header_addr:	resb 4	; This is a self pointer. It must be set to the
							; actual multiboot header address.

	.load_addr:		resb 4	; This is the physical address of the .text section,
							; but it must be less than or equal to header_addr.

	.load_end_addr:	resb 4	; This is the physical address of the end of the
							; .data section. It represent the end of the OS
							; image.

	.bss_end_addr:	resb 4	; This is the physical address of the end of the
							; .bss section. The loader must reserved and clear
							; the memory between the load_end_addr and
							; bss_end_addr for the OS.

	.entry_addr:	resb 4	; This is the entry point of the OS.

	; These feilds are valid if bit 2 of the flags field is set. They provide a
	; way for the OS to specify in witch video mode (VBE) it apreciate to be at
	; startup. The bootloader should try to set it up with the coresponding
	; parameter and if it failed it should try to get the most similar config.
	; The bootloader should always set the VBEs feilds in the multiboot
	; information structure to indicate the current video mode to the OS.
	.mode_type:		resb 4	; This feild indicate the mode type (graphics/text)
							; by setting 0 for graphics mode and 1 for text
							; mode. All other values are reserved.

	.width:			resb 4	; Number of columns, in px for graphic mode and in
							; characters for text mode.

	.height:		resb 4	; Same as above but for line.

	.depth:			resb 4	; The number of bits per px for graphic mode and 0
							; for text mode.
endstruc

;----------------;
; MULTIBOOT INFO ;
;----------------;
struc multibootInfo
	.flags:				resb 4	; This feild provide a list of avalable feild as
								; describe by it's defs.

	.mem_lower:			resb 4	; Represent the memory avalabe bellow 1Mb,
								; provided by the bios.
	.mem_upper:			resb 4	; Represent the avalable memory above 1Mb, also
								; provided by the bios.

	.boot_device:		resb 4	; Boot device and part, bios style.

	.cmdline:			resb 4	; Command line provided by the config file.

	.mods_count:		resb 4	; Number of modules loaded.
	.mods_addr:			resb 4	; Address of the first module entry.

	.syms:				resb 16	; Syms table, not suported yet.

	.mmap_length:		resb 4	; Mem map length provided by the bios.
	.mmap_addr:			resb 4	; Mem map entry address.

	.drives_length:		resb 4	; Everithing bellow is not implemented yet.
	.drives_addr:		resb 4

	.config_table:		resb 4

	.boot_loader_name:	resb 4

	.apm_table:			resb 4

	.vbe_control_info:	resb 4
	.vbe_mode_info:		resb 4
	.vbe_mode:			resb 4
	.vbe_interface_seg:	resb 4
	.vbe_interface_off:	resb 4
	.vbe_interface_len:	resb 4
endstruc

struc apm
	.version:		resb 2
	.cseg:			resb 2
	.offset:		resb 4
	.cseg_16:		resb 2
	.dseg:			resb 2
	.flags:			resb 2
	.cseg_len:		resb 2
	.cseg_16_len:	resb 2
	.dseg_len:		resb 2
endstruc

struc mMap
	[ABSOLUTE -4]
	.size:		resb 4
	.base_addr:	resb 8
	.length:	resb 8
	.type:		resb 4
endstruc

%endif ; MULTIBOOT.INC
