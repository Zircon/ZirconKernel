;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright � 2011-2012 Maxime Jeanson for the Zircon Project.               ;;
;; This file is part of the Zircon Project.                                   ;;
;;                                                                            ;;
;; The Zircon Project is free software: you can redistribute it and/or modify ;;
;; it under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or              ;;
;; (at your option) any later version.                                        ;;
;;                                                                            ;;
;; The Zircon Project is distributed in the hope that it will be useful, but  ;;
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ;;
;; or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ;;
;; for more details.                                                          ;;
;;                                                                            ;;
;; You should have received a copy of the GNU General Public License along    ;;
;; with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; This file is based on the NASM macro c-style function definition.		  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

%ifndef __F_NASM_C32_INC__
%define __F_NASM_C32_INC__
	
	;---------------------------------------------------------------------------
	; The var macro work in the same way of the arg macro, it make it easier to
	; access and create non-global vars in a function.
	;---------------------------------------------------------------------------
	%imacro var 0-2 4,0
		[SECTION .bss]
			ABSOLUTE %$var
			%00_:
		[SECTION .text]
		%xdefine %00 ebp-%00_

		%assign %$var (%1+%$var)
		sub esp, byte %1
	%endmacro

	;---------------------------------------------------------------------------
	; The proc macro MUST be preceed by an endproc instance to work. It push an
	; new proc instance in the preprocessor stack and prepare some structures to
	; increase the flexibility and performance of the procedure.
	;
	; This macro is based on the one defined by the nasm specs.
	;---------------------------------------------------------------------------
	%imacro proc 1
		%push _proc

		%ifnidni %00, private
			global _%1:function
		%endif

		_%1:
		push ebp
		mov ebp,esp
		

		%define %$arg 0x08
		%define %$narg 0
		%define %$var 4
		%define %$procname %00_%1
		%$tmpesp	var
	%endmacro
	
	;---------------------------------------------------------------------------
	; The proc macro MUST be preceed by an endproc instance to work. It push an
	; new proc instance in the preprocessor stack and prepare some structures to
	; increase the flexibility and performance of the procedure.
	;
	; This macro is based on the one defined by the nasm specs.
	;---------------------------------------------------------------------------
	%imacro dproc 1
		%define __PROC_%1
	%endmacro

	;---------------------------------------------------------------------------
	; The arg macro make it easier to access to the args passed to the function.
	;
	; This macro is based on the one defined by the nasm specs.
	;---------------------------------------------------------------------------
	%imacro arg 0-2 4
		[SECTION .bss]
			ABSOLUTE %$arg
			%00_:
		[SECTION .text]
		%define %00 ebp+%00_
		%if %0 = 2
			%define %00(x) x+%2
		%endif

		%assign %$arg (%1+%$arg)
		%assign %$narg (%$narg+1)
	%endmacro

	;---------------------------------------------------------------------------
	; The tab macro simplify the usage of an data table or an buffer. It's first
	; parameter is the name of the table, the second is the size of each case in
	; the table, the third is the number of case in the table and than the
	; optional one, the structure type.
	;---------------------------------------------------------------------------
	%imacro tab 3-4
		%if %0 == 3
			%define %1(x) (x*%2)
		%else
			%define %1(x) (x*%2)+%4
		%endif
	%endmacro

	;---------------------------------------------------------------------------
	; In the same way than the last one, the array macro provide an intuitive
	; way to manage an arry or a buffer. It's first parameter is it's name, than
	; the structure based on, the base address of it and finaly the size of the
	; whole array.
	;---------------------------------------------------------------------------
	%imacro array 4
		%define %1(x) ((x*%2_size)+%3)+%2
	%endmacro

	;---------------------------------------------------------------------------
	; The return macro will set correctly the stack for the leave instruction,
	; place in the eax register the value of the first parameter and than leave
	; the function.
	;---------------------------------------------------------------------------
	%imacro return 0-1 0x00
		mov eax, %1
		leave
		ret
	%endmacro

	;---------------------------------------------------------------------------
	; The pcall macro MUST NOT be use by the user and is part of the c-style
	; function call system. It is use to push correctly the args to the function
	; according to the c-style function call standard.
	;---------------------------------------------------------------------------
	%imacro pcall 1-*
		pushad
		mov [%$tmpesp], esp

		%if %0 != 1
			%rotate -1			; This must be here for the invert push standard.
			%rep %0-1
				push %1
				%rotate -1
			%endrep
		%endif

		mov ebp, [%$tmpesp]
		
		call %1				; Finally, it call the function.

		mov esp, ebp
		%ifidni %00, void
			mov [ebp+0x1c], eax
		%elifidni %00, int
			mov [ebp+0x1c], eax
		%elifidni %00, long
			mov [ebp+0x1c], eax
			mov [ebp+0x14], edx
		%elifidni %00, char
			mov [ebp+0x1c], al
		%elifidni %00, short
			mov [ebp+0x1c], ax
		
		%elifidni %00, eax
			mov [ebp+0x1c], eax
		%elifidni %00, ax
			and eax, 0x0000ffff
			mov [ebp+0x1c], eax
		%elifidni %00, al
			and eax, 0x000000ff
			mov [ebp+0x1c], eax
		%elifidni %00, ah
			and eax, 0x0000ff00
			mov [ebp+0x1c], eax
		
		%elifidni %00, ebx
			mov [ebp+0x10], eax
		%elifidni %00, bx
			and eax, 0x0000ffff
			mov [ebp+0x10], eax
		%elifidni %00, bl
			and eax, 0x000000ff
			mov [ebp+0x10], eax
		%elifidni %00, bh
			and eax, 0x0000ff00
			mov [ebp+0x10], eax
		
		%elifidni %00, ecx
			mov [ebp+0x18], eax
		%elifidni %00, cx
			and eax, 0x0000ffff
			mov [ebp+0x18], eax
		%elifidni %00, cl
			and eax, 0x000000ff
			mov [ebp+0x18], eax
		%elifidni %00, ch
			and eax, 0x0000ff00
			mov [ebp+0x18], eax
		
		%elifidni %00, edx
			mov [ebp+0x14], eax
		%elifidni %00, dx
			and eax, 0x0000ffff
			mov [ebp+0x14], eax
		%elifidni %00, dl
			and eax, 0x000000ff
			mov [ebp+0x14], eax
		%elifidni %00, dh
			and eax, 0x0000ff00
			mov [ebp+0x14], eax

		; EBP, EDI, ESI
		
		%elifidni %00, ebp
			mov [ebp+0x08], eax
		%elifidni %00, bp
			and eax, 0x0000ffff
			mov [ebp+0x08], eax

		%elifidni %00, esi
			mov [ebp+0x04], eax
		%elifidni %00, si
			and eax, 0x0000ffff
			mov [ebp+0x04], eax
			
		%elifidni %00, edi
			mov [ebp+0x00], eax
		%elifidni %00, di
			and eax, 0x0000ffff
			mov [ebp+0x00], eax

		%elifstr %00
			%deftok %$dest %00
			mov %$dest, eax
			%undef %$dest
		%endif

		popad
	%endmacro

	;---------------------------------------------------------------------------
	; The func macro is use to make the last c-style function preparation and to
	; define an external function.
	;---------------------------------------------------------------------------
	%imacro func 1-*
		%ifndef __PROC_%1
		
			%ifnidni %00, private
				extern _%1
			%endif
		%endif

		%define __FUNC_%1

		%if %0 == 1
			%define %1(t) pcall _%1
		%else
			%define %1(%{2:-1}) pcall _%1, %{2:-1}
		%endif
		
	%endmacro

	;---------------------------------------------------------------------------
	; The endproc macro must fallow by an proc instance to work. It only pop the
	; preprocessor stack and define an end of function label to be able of size
	; calculation. Than, it return in the eax, ax, al register the value return
	; by the function.
	;
	; This macro is based on the one defined by the nasm specs.
	;---------------------------------------------------------------------------
	%imacro endproc 0-1
		%ifnctx _proc
			%error Mismatched 'endproc' / 'proc'	; Make sure that it was
													; fallowed by an proc
													; instance.
		%else
			return								; Return the value of the
													; function

			__end_%$procname:						; useful for calculating
													; function size
			%pop
		%endif
	%endmacro

%endif ; __F_NASM_C32_INC__
