;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright � 2011-2012 Maxime Jeanson for the Zircon Project.               ;;
;; This file is part of the Zircon Project.                                   ;;
;;                                                                            ;;
;; The Zircon Project is free software: you can redistribute it and/or modify ;;
;; it under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or              ;;
;; (at your option) any later version.                                        ;;
;;                                                                            ;;
;; The Zircon Project is distributed in the hope that it will be useful, but  ;;
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ;;
;; or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ;;
;; for more details.                                                          ;;
;;                                                                            ;;
;; You should have received a copy of the GNU General Public License along    ;;
;; with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

%ifndef __F_NASM_NASMDEF_inc__
%define __F_NASM_NASMDEF_inc__

	%macro const 1-*
		[SECTION .rodata]
		%{1:-1}
		[SECTION .text]
	%endmacro

	%macro data 1-2
		%ifndef __IDATA_%1
			[extern _%1]
		%endif
		
		%if %0 == 2
			%define %1(x) _%1+%2
		%else
			%define %1(x) _%1
		%endif
	%endmacro

	%macro struc 2-3
		%if %0 == 2
			%ifndef __ISTRUC_%1
				[extern _%1]
			%endif
			%define %1(x) _%1 + %2
		%else
			%ifndef __ISTRUC_%1
				[extern _%1]
			%endif
			%define %1(x) _%1 + x + %2
		%endif
	%endmacro
	
	%macro istruc 2
		[global _%1]
		[SECTION .data]
		_%1: resb %2_size
		[SECTION .text]
	%endmacro
	
	%macro idata 2-3
		%if %0 == 2
			global _%1
			[SECTION .data]
			_%1: resb %2
			[SECTION .text]
		%else
			global _%1
			[SECTION .data]
			%if %2 == 1
				_%1: db %3
			%elif %2 == 2
				_%1: dw %3
			%elif %2 == 4
				_%1: dd %3
			%elif %2 == 8
				_%1: dq %3
			%endif
			[SECTION .text]
		%endif
	%endmacro
	
	%macro uint8 1
		[SECTION .data]
		%1: resb 1
		[SECTION .text]
	%endmacro

	%macro uint16 1
		[SECTION .data]
		%1: resb 2
		[SECTION .text]
	%endmacro

	%macro uint32 1
		[SECTION .data]
		%1: resb 4
		[SECTION .text]
	%endmacro

	%macro uint64 1
		[SECTION .data]
		%1: resb 8
		[SECTION .text]
	%endmacro

	%macro uint8 2
		[SECTION .data]
		%1: db %2
		[SECTION .text]
	%endmacro

	%macro uint16 2
		[SECTION .data]
		%1: dw %2
		[SECTION .text]
	%endmacro

	%macro uint32 2
		[SECTION .data]
		%1: dd %2
		[SECTION .text]
	%endmacro

	%macro uint64 2
		[SECTION .data]
		%1: dq %2
		[SECTION .text]
	%endmacro

	%macro uint8_ptr 2
		[SECTION .data]
		ABSOLUTE %2
		%1: resb 1
		[SECTION .text]
	%endmacro

	%macro uint16_ptr 2
		[SECTION .data]
		ABSOLUTE %2
		%1: resb 2
		[SECTION .text]
	%endmacro

	%macro uint32_ptr 2
		[SECTION .data]
		ABSOLUTE %2
		%1: resb 4
		[SECTION .text]
	%endmacro

	%macro uint64_ptr 2
		[SECTION .data]
		ABSOLUTE %2
		%1: resb 8
		[SECTION .text]
	%endmacro

	%macro void_ptr 3
		[SECTION .data]
		ABSOLUTE %2
		%1: resb %3
		[SECTION .text]
	%endmacro

	%macro str 2
		[SECTION .rodata]
		%1: db %2, 0x00
		[SECTION .text]
	%endmacro

	%macro bfr 2
		[SECTION .data]
		%1: resb %2
		[SECTION .text]
	%endmacro

%endif ; __F_NASM_NASMDEF_inc__