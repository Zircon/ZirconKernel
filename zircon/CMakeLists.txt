SET(CMAKE_ASM_NASM_OBJECT_FORMAT elf32)
SET(CMAKE_ASM_NASM_LINKER cc)
SET(CMAKE_ASM_NASM_COMPILE_OBJECT "")
SET(CMAKE_ASM_NASM_LINK_EXECUTABLE "${CMAKE_ASM_NASM_LINKER} <FLAGS> <CMAKE_ASM_NASM_LINK_FLAGS> <LINK_FLAGS> <OBJECTS> -o <TARGET> <LINK_LIBRARIES>")
SET(CMAKE_ASM_NASM_CREATE_SHARED_LIBRARY "${CMAKE_ASM_NASM_LINKER} <CMAKE_C_LINK_FLAGS> <LINK_FLAGS> <OBJECTS> -o <TARGET>")
SET(CMAKE_DEPFILE_FLAGS_ASM_NASM "-M -MD <DEP_FILE> -MT <DEP_TARGET>")

include(${CMAKE_SOURCE_DIR}/cmake/source.cmake)

_create_target(
    NAME zircon-configuration
    TYPE INTERFACE_LIBRARY
    NAMESPACE zircon::
    COMPONENT core
    TARGET zircon

    PUBLIC_HEADERS
        ../include/nasm/nasm

    PUBLIC_COMPILE_OPTIONS
        $<$<COMPILE_LANGUAGE:ASM_NASM>:-P${CMAKE_SOURCE_DIR}/include/nasm/nasm>
        $<$<COMPILE_LANGUAGE:C>:-march=i386>
        $<$<COMPILE_LANGUAGE:C>:-m32>
        $<$<COMPILE_LANGUAGE:C>:-fno-pie>
        $<$<COMPILE_LANGUAGE:CXX>:-march=i386>
        $<$<COMPILE_LANGUAGE:CXX>:-m32>
        $<$<COMPILE_LANGUAGE:CXX>:-fno-pie>

    PUBLIC_LINK_OPTIONS
        -Wl,-Map=${CMAKE_CURRENT_BINARY_DIR}/memmap.txt
        -m32
        -Wl,-melf_i386
        -Wl,--allow-multiple-definition
        -Wl,--oformat=binary
        -Wl,--no-demangle
        -nostartfiles
        -nodefaultlibs
)

add_subdirectory(stage1.pro)
add_subdirectory(stage1_5.pro)

add_custom_target(
    zircon
        # Create device
        COMMAND dd if=/dev/zero of=${CMAKE_CURRENT_BINARY_DIR}/sda bs=512 count=4096
        COMMAND parted --script ${CMAKE_CURRENT_BINARY_DIR}/sda mklabel msdos mkpart primary fat32 1s 4095s set 1 boot on

        # Create partition
        COMMAND dd if=/dev/zero of=${CMAKE_CURRENT_BINARY_DIR}/sda1 bs=512 count=4095
        COMMAND mkfs.vfat -R 128 ${CMAKE_CURRENT_BINARY_DIR}/sda1

        # Copy stage 1
        COMMAND dd if=${CMAKE_CURRENT_BINARY_DIR}/stage1.pro/stage-1.0 of=${CMAKE_CURRENT_BINARY_DIR}/sda bs=440 count=1 conv=notrunc

        # Copy stage 1.5
        COMMAND dd if=${CMAKE_CURRENT_BINARY_DIR}/stage1_5.pro/stage-1.5 of=${CMAKE_CURRENT_BINARY_DIR}/sda1 seek=1 bs=512 conv=notrunc

        # Assemble partition into device
        COMMAND dd if=${CMAKE_CURRENT_BINARY_DIR}/sda1 of=${CMAKE_CURRENT_BINARY_DIR}/sda seek=1 conv=notrunc

    DEPENDS stage-1.0
    DEPENDS stage-1.5
)