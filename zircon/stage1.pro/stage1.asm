;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright © 2011-2012 Maxime Jeanson for the Zircon Project.               ;;
;; This file is part of the Zircon Project.                                   ;;
;;                                                                            ;;
;; The Zircon Project is free software: you can redistribute it and/or modify ;;
;; it under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or              ;;
;; (at your option) any later version.                                        ;;
;;                                                                            ;;
;; The Zircon Project is distributed in the hope that it will be useful, but  ;;
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ;;
;; or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ;;
;; for more details.                                                          ;;
;;                                                                            ;;
;; You should have received a copy of the GNU General Public License along    ;;
;; with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[BITS 16]

%include "stage1.inc"

start:
    ; We save the drive of witch we load the current loader.
    mov [drive], dl

    ; We set the stack. Actually, we don't use it at all, this will probably be
    ; removed in future version according to the need of space.
    xor eax, eax
    mov ss, ax
    mov ds, ax
    mov es, ax
    mov sp, 0x8000

    ; We enable A20 line
    mov eax, 0x2401
    int 0x15

    ; Resetting drive
    reset:
        mov ah, 0x00
        mov dl, [drive]
        int 0x13
        jc reset

    ; We search for the MBR partition witch have the bootable flag active.
    setActivePart:
        xor edx, edx

        .search:
            mov al, [part1.status+edx]
            cmp al, 0x80
            je .found

            cmp edx, 0x40
            ja .notFound

            add edx, 0x10
            jmp .search

        .notFound:
            mov ebp, st.noBootableDrive

            jmp halt

        .found:
            ; We set the LBA start of the part
            mov eax, [part1.lba+edx]
            mov [boot.lba], eax

            ; We set the size of the part
            mov eax, [part1.size+edx]
            mov [boot.size], eax

    ; At this point we assume that the bootable partition is an FAT part but
    ; tests must be add to improve portability and ensure that unrecoverable
    ; boot are eliminated.
    readStart:
        mov [dap.count], word 1
        mov [dap.seg], ds
        mov [dap.offset], word 0x8000
        mov eax, [boot.lba]
        mov [dap.lbaLo], eax
        mov ah, 0x42
        mov dl, [drive]
        mov si, dap
        int 0x13
        jc readStart

    ; We load the stage2 bootloader witch is placed in the reserved area of the
    ; boot partition. This work only for FAT part.
    readStage2:
        mov ax, [FAT.rsvdSecCnt]
        mov [dap.count], ax
        mov [dap.seg], ds
        mov [dap.offset], word 0x8200
        mov eax, [boot.lba]
        inc eax
        mov [dap.lbaLo], eax
        mov ah, 0x42
        mov dl, [drive]
        mov si, dap
        int 0x13
        jc readStage2

    ; We setup some information for the stage2, we verify if the header is
    ; valid and continue.
    setup:
        mov eax, [stage2.magic]
        cmp eax, 0x4645425A
        mov ebp, st.magicNumberError
        jne halt

        mov al, [drive]
        mov [stage2.bootDisk], al

        mov eax, [boot.lba]
        mov [stage2.lba], eax

        mov eax, [boot.size]
        mov [stage2.size], eax

        mov esi, [stage2.entry]
        sub esi, 0x8200
        mov ax, 0x0820
        mov ds, ax

        jmp 0x0800:0x0220
;
;        jmp $ + 2
;
;        cli
;        lgdt [gdtr]
;        mov eax, cr0
;        or eax, 1
;        mov cr0, eax
;
;        jmp 0x08:stage2_entry

halt:
    xor ecx, ecx
    mov cl, byte [ebp]
    inc ebp
    mov ah, 0x13
    mov al, 0x01
    mov bl, 0X4F
    mov dh, 0x00
    mov dl, 0x00
    int 0x10

    hlt
    jmp $

;===============================================================================
; DATA
;===============================================================================

drive:          db 0x00

boot:
    .lba:	    dd 0x00
    .size:	    dd 0x00

dap:
                db 0x10
                db 0x00
    .count:		dw 0x00
    .offset:	dw 0x00
    .seg:		dw 0x00
    .lbaLo:		dd 0x00
    .lbaHi:		dd 0x00

st:
    .noBootableDrive:   db 36, "ERROR: Could not find bootable drive"
    .magicNumberError:  db 61, "ERROR: Could not load STAGE 2 because magic number is invalid"

;gdt:
;    .ns: dd 0x00, 0x00
;    .cs: dw 0xFFFF, 0x0000
;         db 0x00, 10011010b, 11001111b, 0x00
;    .ds: dw 0xFFFF, 0x0000
;         db 0x00, 10010010b, 11001111b, 0x00
;gdt_end:
;
;gdtr:
;	dw gdt_end - gdt - 1
;	dd gdt
;
;BITS 32
;
;stage2_entry:
;	mov		ax, 0x10		; set data segments to data selector (0x10)
;	mov		ds, ax
;	mov		ss, ax
;	mov		es, ax
;	mov		esp, 90000h		; stack begins from 90000h
;
;	mov [0xb8000], dword 0x07690748
;
;    jmp esi



times (440-($-$$)) db 0	; We have to be 440 bytes. Clear the rest of the bytes
                        ; with 0

;===============================================================================
; BSS
;===============================================================================

[SECTION .bss]
    diskSignature:		resb 4
                        resb 2
    part1:
        .status:		resb 1
        .head:			resb 1
        .sector:		resb 1
        .cylinder:		resb 1
        .type:			resb 1
        .headEnd:		resb 1
        .sectorEnd:		resb 1
        .cylinderEnd:	resb 1
        .lba:			resb 4
        .size:			resb 4

    part2:
        .status:		resb 1
        .head:			resb 1
        .sector:		resb 1
        .cylinder:		resb 1
        .type:			resb 1
        .headEnd:		resb 1
        .sectorEnd:		resb 1
        .cylinderEnd:	resb 1
        .lba:			resb 4
        .size:			resb 4

    part3:
        .status:		resb 1
        .head:			resb 1
        .sector:		resb 1
        .cylinder:		resb 1
        .type:			resb 1
        .headEnd:		resb 1
        .sectorEnd:		resb 1
        .cylinderEnd:	resb 1
        .lba:			resb 4
        .size:			resb 4

    part4:
        .status:		resb 1
        .head:			resb 1
        .sector:		resb 1
        .cylinder:		resb 1
        .type:			resb 1
        .headEnd:		resb 1
        .sectorEnd:		resb 1
        .cylinderEnd:	resb 1
        .lba:			resb 4
        .size:			resb 4

    MBRSignature:		resb 2

    stack:				resb 512


    ; FAT sector 0 organization.
    [ABSOLUTE FAT_SECT_0]
    FAT:
                        resb 3
        .oemName:		resb 8
        .bytsPerSec:	resb 2
        .secPerClus:	resb 1
        .rsvdSecCnt:	resb 2
        .numFATs:		resb 1
        .rootEntCnt:	resb 2
        .totSec16:		resb 2
        .media:			resb 1
        .FATSz16:		resb 2
        .secPerTrk:		resb 2
        .numHeads:		resb 2
        .hiddSec:		resb 4
        .totSec32:		resb 4
        .FATSz32:		resb 4
        .extFlags:		resb 2
        .fsVer:			resb 2
        .rootClus:		resb 4
        .fsInfo:		resb 2
        .bkBootSec:		resb 2
                        resb 12
        .drvNum:		resb 1
                        resb 1
        .bootSig:		resb 1
        .volID:			resb 4
        .volLab:		resb 11
        .filSysType:	resb 8

    ; Stage 2 header
    [ABSOLUTE STAGE2]
    stage2:
        .magic:			resb 4
        .entry:			resb 4
        .bootDisk:		resb 1
        .lba:			resb 4
        .size:			resb 4
        .type:			resb 1
