;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright © 2011-2012 Maxime Jeanson for the Zircon Project.               ;;
;; This file is part of the Zircon Project.                                   ;;
;;                                                                            ;;
;; The Zircon Project is free software: you can redistribute it and/or modify ;;
;; it under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or              ;;
;; (at your option) any later version.                                        ;;
;;                                                                            ;;
;; The Zircon Project is distributed in the hope that it will be useful, but  ;;
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ;;
;; or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ;;
;; for more details.                                                          ;;
;;                                                                            ;;
;; You should have received a copy of the GNU General Public License along    ;;
;; with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

%ifndef APM_INC
%define APM_INC

getAPM:
	call getAPM_version
	jc .error

	call getAPM_setDriver
	jc .error

	call getAPM_connection
	jc .error

	mov esi, [multiboot]
	or dword [multInfo(si).flags], MBI_FLAGS_APM_TABLE
	mov dword [multInfo(si).apm_table], multInfoApm()

	.error:
		ret


getAPM_version:
	mov eax, 0x5300
	xor ebx, ebx
	int 0x15
	jc .error

	mov [multInfoApm().version], ax
	mov [multInfoApm().flags], cx

	ret

	.error:
		stc
		ret

getAPM_connection:
	mov eax, 0x5303
	xor ebx, ebx
	int 0x15
	jc .error

	mov [multInfoApm().cseg], ax
	mov [multInfoApm().offset], ebx
	mov [multInfoApm().cseg_16], cx
	mov [multInfoApm().dseg], dx

	cmp word [multInfoApm().version], 0x0100
	ja .v1.x

	ret

	.v1.x:
		mov [multInfoApm().cseg_len], si
		mov [multInfoApm().cseg_16_len], si
		mov [multInfoApm().dseg_len], di

		ret

	.error:
		stc
		ret


getAPM_setDriver:
	mov eax, 0x530e
	xor ebx, ebx
	mov cx, [multInfoApm().version]
	int 0x15
	jc .error

	ret

	.error:
		stc
		ret

%endif ; APM_INC
