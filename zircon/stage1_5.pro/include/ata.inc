

func ata_read, sector, count, dst
func ata_read_status
func ata_reset

ENUM ATA_STATUS
    ERR      MASK 00000001b
    DRQ      MASK 00010000b
    DWE      MASK 00010000b
    SERV     MASK 00010000b
    SE       MASK 00100000b
    DRDY     MASK 01000000b
    BSY      MASK 10000000b

    RESERVED MASK 00001110b
END