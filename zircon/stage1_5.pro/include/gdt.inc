%ifndef __GDT_INC__
%define __GDT_INC__

%define __GDTR_LIMITE__ 0x00000000
%define __GDTR_BASE__   0x00000002

func gdt_init
func gdt_add, start_ptr, limit, attr

%endif ; __GDT_INC__
