;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright © 2011-2012 Maxime Jeanson for the Zircon Project.               ;;
;; This file is part of the Zircon Project.                                   ;;
;;                                                                            ;;
;; The Zircon Project is free software: you can redistribute it and/or modify ;;
;; it under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or              ;;
;; (at your option) any later version.                                        ;;
;;                                                                            ;;
;; The Zircon Project is distributed in the hope that it will be useful, but  ;;
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ;;
;; or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ;;
;; for more details.                                                          ;;
;;                                                                            ;;
;; You should have received a copy of the GNU General Public License along    ;;
;; with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

%ifndef GET_MEMORY_INC
%define GET_MEMORY_INC

;=========================;
; GET MEM FUNCTIONS CALLS ;
;=========================;
getMem:
	call getMem_E881
	jnc .end

;	call getMem_E801
;	jnc .end

	.end:
		ret

;-------------------------------------------------------------------------------
; This function provide a way to know the size of memory. The EAX and ECX
; registers are both in Kb starting at 1Mb. The EBX and EDX registers are both
; in 64Kb and they start at 16Mb.
;
; To manage correctly this function, we have to validate the content of the
; registers.
;-------------------------------------------------------------------------------
getMem_E881:
	; We clear every entries.
	xor ebx, ebx
	xor ecx, ecx
	xor edx, edx

	; This is function 0xE881 of int 0x15.
	mov eax, 0xe881
	int 0x15
	jc .error

	; We validate by checking if the ECX register is empty or not (empty = use
	; EAX/EBX pair ELSE use ECX/EDX pair.
	.isValid?:
		jecxz .isValid

		mov eax, ecx
		mov ebx, edx

	; When the content is valid, we copy it to the multiboot info structure.
	.isValid:
		mov edi, [multiboot]
		or dword [multInfo(edi).flags], MBI_FLAGS_MEM
		mov [multInfo(edi).mem_lower], dword 640
		; NOTE: the EBX/EDX pair is in 64Kb...
		shl ebx, 6
		add ebx, eax
		mov [multInfo(edi).mem_upper], ebx

		clc
		ret

	; We set the cary flag on error.
	.error:
		stc
		ret

;-------------------------------------------------------------------------------
; Same as above but with 16bits registers.
;-------------------------------------------------------------------------------
getMem_E801:
	mov eax, 0xe801
	xor ebx, ebx
	xor ecx, ecx
	xor edx, edx
	int 0x15
	jc .error

	.isValid?:
		jecxz .isValid

		mov eax, ecx
		mov ebx, edx

	.isValid:
		mov edi, [multiboot]
		or dword [multInfo(edi).flags], MBI_FLAGS_MEM
		mov [multInfo(edi).mem_lower], dword 640
		; NOTE: the EBX/EDX pair is in 64Kb...
		shl ebx, 6
		add ebx, eax
		mov [multInfo(edi).mem_upper], ebx

		clc
		ret

	.error:
		stc
		ret

%endif ; GET_MEMORY_INC
