;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright © 2011-2012 Maxime Jeanson for the Zircon Project.               ;;
;; This file is part of the Zircon Project.                                   ;;
;;                                                                            ;;
;; The Zircon Project is free software: you can redistribute it and/or modify ;;
;; it under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or              ;;
;; (at your option) any later version.                                        ;;
;;                                                                            ;;
;; The Zircon Project is distributed in the hope that it will be useful, but  ;;
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ;;
;; or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ;;
;; for more details.                                                          ;;
;;                                                                            ;;
;; You should have received a copy of the GNU General Public License along    ;;
;; with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; TODO: EVERYTHING here HAVE to be tested, optimized AND commented

struc mallocEntry
	.prev:	resb 2
	.flag:	resb 2
	.size:	resb 2
	.next:	resb 2
endstruc

%define ME_FLAG_FREE	0x0000
%define ME_FLAG_USED	0x0001

tmpRet: dq 0
params:	dd 0, 0, 0, 0, 0, 0

malloc_init:
	pusha

	mov edi, 0x500
	mov esi, 0x7c00-mallocEntry_size

	mov [edi+mallocEntry.prev], word -1
	mov [edi+mallocEntry.flag], word ME_FLAG_FREE
	mov [edi+mallocEntry.size], word (0x7c00-0x500)-(2*mallocEntry_size)
	mov [edi+mallocEntry.next], word (0x7c00-mallocEntry_size)

	mov [esi+mallocEntry.prev], word di
	mov [esi+mallocEntry.flag], word ME_FLAG_USED
	mov [esi+mallocEntry.size], word (0x10000-0x7c00-mallocEntry_size)
	mov [esi+mallocEntry.next], word -1

	popa
	ret


; EAX = size
malloc:
	pusha

	call malloc_find
	jc .error

	mov edi, [tmpRet]
	call malloc_alloc

	.error:

	popa
	clc
	mov eax, [tmpRet]
	ret

malloc_alloc:
	pusha

	add eax, 8
	mov esi, eax
	add esi, edi
	cmp [edi+mallocEntry.size], ax
	je .isEqual
	jmp .isTooLarge

	.isTooLarge:
		mov dx, [edi+mallocEntry.next]
		mov [edi+mallocEntry.next], ax
		add [edi+mallocEntry.next], di
		mov bx, [edi+mallocEntry.size]
		sub ebx, eax
		mov [edi+mallocEntry.size], ax
		sub [edi+mallocEntry.size], word 8
		mov [edi+mallocEntry.flag], word ME_FLAG_USED

		mov [esi+mallocEntry.prev], word di
		mov [esi+mallocEntry.flag], word ME_FLAG_FREE
		mov [esi+mallocEntry.size], word bx
		mov [esi+mallocEntry.next], word dx

		jmp .ret

	.isEqual:
		mov [edi+mallocEntry.flag], word ME_FLAG_USED

		jmp .ret


	.ret:
		mov [tmpRet], edi
		add [tmpRet], dword 8
		popa
		clc
		ret


malloc_find:
	pusha

	add eax, 8
	xor edi, edi
	mov edi, 0x500
	.search:
		cmp [di+mallocEntry.flag], word ME_FLAG_USED
		je .next

		cmp [di+mallocEntry.size], word ax
		jae .found

	.next:
		cmp [di+mallocEntry.next], word -1
		je .endOfMem

		mov di, [di+mallocEntry.next]
		jmp .search

	.endOfMem:
		popa
		stc
		ret

	.found:
		mov [tmpRet], edi

		popa
		clc
		ret

;===============================================================================

; EAX = mem*
free:
	pusha

	mov esi, eax
	sub esi, 8
	call free_merge

	popa
	ret

; ESI = curr
free_merge:
	pusha

	xor edx, edx
	xor edi, edi
	mov di, [si+mallocEntry.prev]
	mov dx, [si+mallocEntry.next]

	cmp di, 0xffff
	je .next

	.prev:
		cmp [di+mallocEntry.flag], word ME_FLAG_FREE
		jne .next

		call free_merging

		mov al, 'M'

	.next:
		cmp [edx+mallocEntry.flag], word ME_FLAG_FREE
		jne .end_merging

		cmp al, 'M'
		je .prev_did

			mov edi, esi
			mov esi, edx
			mov dx, [si+mallocEntry.next]

			jmp .next_do

		.prev_did:
		mov esi, edx
		mov dx, [si+mallocEntry.next]

		.next_do:


		call  free_merging

		mov ah, 'M'

	.end_merging:
		cmp ax, 'MM'
		je .end

		cmp ah, 'M'
		je .merged_next

		jmp .notMerged

	.merged_next:
		mov [di+mallocEntry.flag], word ME_FLAG_FREE

		jmp .end

	.notMerged:
		mov [si+mallocEntry.flag], word ME_FLAG_FREE

	.end:
		popa
		clc
		ret

; EDX = next
; ESI = curr
; EDI = prev
free_merging:
	pusha

	mov ax, [esi+mallocEntry.next]
	mov [edi+mallocEntry.next], ax
	mov [edx+mallocEntry.prev], di

	mov ax, [esi+mallocEntry.size]
	add ax, [edi+mallocEntry.size]
	add ax, 0x8
	mov [edi+mallocEntry.size], ax

	popa
	ret


