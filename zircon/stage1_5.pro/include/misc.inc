;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright © 2011-2012 Maxime Jeanson for the Zircon Project.               ;;
;; This file is part of the Zircon Project.                                   ;;
;;                                                                            ;;
;; The Zircon Project is free software: you can redistribute it and/or modify ;;
;; it under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or              ;;
;; (at your option) any later version.                                        ;;
;;                                                                            ;;
;; The Zircon Project is distributed in the hope that it will be useful, but  ;;
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ;;
;; or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ;;
;; for more details.                                                          ;;
;;                                                                            ;;
;; You should have received a copy of the GNU General Public License along    ;;
;; with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

%ifndef MISC_INC
%define MISC_INC

setBootDev:
	mov al, [bootDisk]
	or eax, 0xffffff00
	mov edi, [multiboot]
	or dword [multInfo(edi).flags], MBI_FLAGS_BOOT_DEV
	mov [multInfo(edi).boot_device], eax

	ret

setBootloaderName:
	mov esi, [multiboot]
	or dword [multInfo(si).flags], MBI_FLAGS_BL_NAME
	mov [multInfo(si).boot_loader_name], dword zirconBootloaderName

	ret

getConfig:
	xor eax, eax
	xor ebx, ebx

	mov ah, 0xc0
	int 0x15
	jc .error

	xor eax, eax
	and ebx, 0xffff
	mov ax, es
	shl eax, 4
	add eax, ebx

	mov esi, [multiboot]
	or dword [multInfo(si).flags], MBI_FLAGS_CFG_TABLE
	mov [multInfo(si).config_table], eax

	clc
	ret

	.error:
		stc
		ret

%endif ; MISC_INC
