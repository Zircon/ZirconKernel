;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright © 2011-2012 Maxime Jeanson for the Zircon Project.               ;;
;; This file is part of the Zircon Project.                                   ;;
;;                                                                            ;;
;; The Zircon Project is free software: you can redistribute it and/or modify ;;
;; it under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or              ;;
;; (at your option) any later version.                                        ;;
;;                                                                            ;;
;; The Zircon Project is distributed in the hope that it will be useful, but  ;;
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ;;
;; or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ;;
;; for more details.                                                          ;;
;;                                                                            ;;
;; You should have received a copy of the GNU General Public License along    ;;
;; with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

%ifndef MMAP_INC
%define MMAP_INC

getMMap:
	mov ebx, 0x0000
	mov ecx, 0x18

	mov eax, 0x1000
	call malloc
	mov edi, eax
	mov esi, [multiboot]
	mov [multInfo(esi).mmap_addr], dword edi
	add [multInfo(esi).mmap_addr], dword multInfoMMap(0)
	add edi, 8
	mov esi, eax
	.L0:
		call getMMap_read

		test ebx, ebx
		je .end

		add edi, ecx
		add edi, 4

		jmp .L0

	.end:
		mov eax, [multInfoMMap(esi).size-4]
		mov esi, [multiboot]
		or dword [multInfo(esi).flags], MBI_FLAGS_MMAP
		mov [multInfo(esi).mmap_length], eax

		ret

getMMap_read:
	mov eax, 0xe820
	mov edx, 0x534d4150
	int 0x15
	jc .error

	mov dword [edi+mMap.size], ecx
	add dword [edi+mMap.size], 4

	add dword [multInfoMMap(esi).size-4], ecx
	add dword [multInfoMMap(esi).size-4], 4

	ret

	.error:
		stc
		ret

%endif ; MMAP_INC
