[BITS 32]

%include "ata.inc"

IMPORT puts
IMPORT fill_rect
IMPORT puth
IMPORT putcn

IMPORT ata_set_status_indicator

func putcn, char, attr, count, x, y
func puts, ptr, attr, x, y
func fill_rect, x, y, w, h, char, attr
func puth, integer, attr, x, y
func ata_set_status_indicator, disk, status

PROC ata_read_status
    ata_set_status_indicator(0, 3)

    DO
        xor eax, eax
        mov dx, 0x01F7
        in al, dx

        puth(DWORD eax, 0x03, 72, 7)

        IF (al NE ATA_STATUS.ERR)
            puts(ATA_READ_ERROR + 4, 0x03, 3, 8)
        END

        IF (al NE ATA_STATUS.DRQ)
            puts(ATA_READ_SERVICE_REQUEST + 4, 0x03, 3, 13)
        END

        IF (al NE ATA_STATUS.SE)
            puts(ATA_READ_DRIVE_FAULT + 4, 0x03, 3, 9)
        END

        IF (al NE ATA_STATUS.DRDY)
            puts(ATA_READ_NOT_READY + 4, 0x03, 3, 10)
        END

        IF (al NE ATA_STATUS.BSY)
            puts(ATA_READ_BUSY + 4, 0x03, 3, 11)
        END
    WHILE (al NE ATA_STATUS.DRDY | ATA_STATUS.DRQ)

    ata_set_status_indicator(0, 4)

    return
endproc

proc ata_reset
    ata_set_status_indicator(0, 2)

    mov dx, 0x03F6
    mov al, 4
    out dx, al
    xor eax, eax
    or eax, 00000010b
    out dx, al

    in al, dx
    in al, dx
    in al, dx
    in al, dx

    push edx
    ata_set_status_indicator(0, 3)
    pop edx

    LABEL .ready
        in al, dx
        and al, 0xc0
        cmp al, 0x40
        jne .ready

    ata_set_status_indicator(0, 4)

    return
endproc

proc ata_read
    %$sector arg
    %$count  arg
    %$dst    arg

    %$error  var

    fill_rect(0, 7, 80, 8, ' ', 0x07)
    puts(ATA_RESET + 4, 0x03, 3, 7)

    ata_reset()

    fill_rect(0, 7, 80, 8, ' ', 0x07)
    puts(ATA_SEND_COMMAND + 4, 0x03, 3, 7)

    ata_set_status_indicator(0, 2)

    LABEL .lba_24
        mov dx, 0x01F6
        mov al, [%$sector + 3]
        or al, 11100000b     ; Set bit 6 in al for LBA mode
        out dx, al

    ata_read_status()

    ata_set_status_indicator(0, 2)

    mov dx, 0x01F1
    xor al, al
    out dx, al

    LABEL .count
        mov dx, 0x01F2
        mov eax, [%$count]
        out dx, al

    LABEL .lba_0
        mov dx, 0x01F3
        mov al, [%$sector]
        out dx, al

    LABEL .lba_8
        mov dx, 0x01F4
        mov al, [%$sector + 1]
        out dx, al

    LABEL .lba_16
        mov dx, 0x01F5
        mov al, [%$sector + 2]
        out dx, al

    mov dx, 0x01F7
    mov al, 0x20
    out dx, al

    LABEL .wait
        fill_rect(0, 7, 80, 11, ' ', 0x07)

        puts(ATA_WAITING_READY + 4, 0x03, 3, 7)

        ata_read_status()

    LABEL .ready
        fill_rect(0, 7, 80, 11, ' ', 0x07)
        puts(ATA_READING + 4, 0x03, 3, 7)

        ata_set_status_indicator(0, 0)

        mov ecx, [%$count]           ; read CL sectors
        shl ecx, 8

        mov edx, 0x1F0       ; Data port, in and out
        mov edi, [%$dst]

    LABEL .read
        rep insw             ; in to [RDI]

    LABEL .return
        fill_rect(0, 7, 80, 8, ' ', 0x07)
        puts(ATA_READ_COMPLETED + 4, 0x03, 3, 7)

        ata_set_status_indicator(0, 4)

        return
endproc

str ATA_RESET, "RESETING ATA DISK"
str ATA_READ_COMPLETED, "ATA DISK READ COMPLETED"
str ATA_READING, "ATA DISK READING"
str ATA_WAITING_READY, "ATA DISK WAITING FOR STATUS TO BE READY"
str ATA_SEND_COMMAND, "SENDING COMMAND ATA DISK"
str ATA_READ_NOT_READY, "ATA DISK IS NOT READY"
str ATA_READ_BUSY, "ATA DISK IS BUSY"
str ATA_READ_ERROR, "COULD NOT READ ATA DISK BECAUSE AN ERROR OCCURED: "
str ATA_READ_SERVICE_REQUEST, "ATA DISK REQUIRES SERVICING"
str ATA_READ_DRIVE_FAULT, "COULD NOT READ ATA DISK BECAUSE A DRIVE FAULT OCCURED"
