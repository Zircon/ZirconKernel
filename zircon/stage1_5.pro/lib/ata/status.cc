
enum class ATA_STATUS_INDICATOR {
    READ,
    WRITE,
    COMMAND,
    STATUS,
    IDLE
};

extern "C" void _putcn(char character, char attribute, int count, int x, int y);

extern "C" int _ata_set_status_indicator(const unsigned int disk, const ATA_STATUS_INDICATOR status) {
    if (disk > 4)
        return -1;

    char character;
    char attribute;

    switch (status) {
        case ATA_STATUS_INDICATOR::IDLE:
            character = 0x7F;
            attribute = 0x87;
            break;

        case ATA_STATUS_INDICATOR::COMMAND:
            character = 0x09;
            attribute = 0x8C;
            break;

        case ATA_STATUS_INDICATOR::STATUS:
            character = 0x09;
            attribute = 0x8A;
            break;

        case ATA_STATUS_INDICATOR::WRITE:
            character = 0x07;
            attribute = 0x8C;
            break;

        case ATA_STATUS_INDICATOR::READ:
            character = 0x07;
            attribute = 0x8A;
            break;
    }

    _putcn(character, attribute, 1, (disk << 1) + 70, 24);

    return 0;
}