;---------------------------------------------------------------------------------------------------
; File name:        gdt.asm
; Version:          1.0.0.0
; Author:           Maxime Jeanson
; Copyright:        (c) 2010 by Sep System, all rights reserved.
;
; Documentations:   file:///usr/doc/system/kernel/gdt/index.html
;
; Contains:         This file contain functions to initialize and manage the global descriptor table
;                   of the kernel for the intel 32 bits architecture.
;
;                   These functions are in this file:
;                    - __gdt.init__:
;                    - __gdt.add.desc__:
;---------------------------------------------------------------------------------------------------

%ifndef __GDT_ASM__
%define __GDT_ASM__

[BITS 32]

SECTION .boot

%include "gdt.inc"

;-----------------------------------------------------------------------------------------------
; This function initialize the Global Descriptors Table Register, it take 0 parameters and need
; to be run before the other functions in this file.
;-----------------------------------------------------------------------------------------------
proc gdt_init
    ; Initialisation
    mov [__GDTR_LIMITE__], word  0x0007     ; Write the limit of the gdt.
    mov [__GDTR_BASE__],   dword 0x00000000 ; Write the base address of the gdt.

    return
endproc

;-----------------------------------------------------------------------------------------------
; This function add an descriptor inside the Global Descriptors Table and set it's value.
; Parameters:
;  - eax: Base address 32 bits.
;  - ebx: Limit 20 bits.
;  - cx:  Attribute 12 bits.
;         11  10  9   8   7   6   5   4            0
;         +---+---+---+---+---+---+---+------------+
;         |   | D |   | A |   | D |   |            |
;         | G | / | 0 | V | P | P | S |    type    |
;         |   | B |   | L |   | L |   |            |
;         +---+---+---+---+---+---+---+------------+
;         G:    Indicate if the limit is in byte ( 0 ) or in pages of 4 ko.
;         D/B:  Indicate if this descriptor work with 16 bits data ( 0 ) or 32 bits data ( 1 ).
;         AVL:  This bits is not use by the processor so you can use it for anything.
;         P:    Indicate if this segment is present ( 1 ) or not in the physical memory.
;         DPL:  Indicate the privilege level of the segment. Root ( 00 ) to users ( 11 ).
;         S:    Indicate if it's a system descriptor ( 0 ) or a segment descriptor ( 1 ).
;         type: Indicate what is the type of descriptor ( code, data or stack ).
;-----------------------------------------------------------------------------------------------
proc gdt_add
    ; Initialization
    xor edx, edx
    mov dx, word [ __GDTR_LIMITE__ ] ; Read the current value of the limit in the gdtr.

    ; Writing
    inc edx         ; Increment the pointer position.

    mov [ edx ], bx ; Write the limit 0-15.
    shr ebx, 16
    add edx, 2      ; Add 2 to the pointer.

    mov [ edx ], ax ; Write the base 16-31.
    shr eax, 16     ; Shift right by 16 the eax register.
    add edx, 2      ; Add 2 to the pointer.

    mov [ edx ], al ; Write the base 32-39.
    inc edx         ; Increment the pointer position.

    mov [ edx ], cl ; Write the attribute 40-47.
    inc edx         ; Increment the pointer position.

    shl ch, 4
    and bl, 0x0f
    and ch, 0xf0
    add ch, bl
    mov [ edx ], ch ; Write the limit 48-51 & the attribute 52-55.
    inc edx         ; Increment the pointer position.

    mov [ edx ], ah ; Write the base 56-63.

    ; Saving
    add word [__GDTR_LIMITE__], 0x8     ; Add one descriptor to the limit of the GDTR.

    return
endproc

proc gdt_setCallGate_desc
    ; Initialization
    xor edx, edx
    mov dx, word [ __GDTR_LIMITE__ ] ; Read the current value of the limit in the gdtr.

    ; Writing
    inc edx         ; Increment the pointer position.

    mov [ edx ], bx ; Write the limit 0-15.
    shr ebx, 16
    add edx, 2      ; Add 2 to the pointer.

    mov [ edx ], ax ; Write the base 16-31.
    shr eax, 16     ; Shift right by 16 the eax register.
    add edx, 2      ; Add 2 to the pointer.

    mov [ edx ], cx ; Write the base 32-39.
    add edx, 2      ; Add 2 to the pointer.

    mov [ edx ], bx ; Write the attribute 40-47.
    add edx, 2      ; Add 2 to the pointer.

    ; Saving
    add word [__GDTR_LIMITE__], 0x8     ; Add one descriptor to the limit of the gdtr.

    return
endproc

%endif ; __GDT_ASM__
