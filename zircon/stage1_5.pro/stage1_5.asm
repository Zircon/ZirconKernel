;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright © 2011-2012 Maxime Jeanson for the Zircon Project.               ;;
;; This file is part of the Zircon Project.                                   ;;
;;                                                                            ;;
;; The Zircon Project is free software: you can redistribute it and/or modify ;;
;; it under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or              ;;
;; (at your option) any later version.                                        ;;
;;                                                                            ;;
;; The Zircon Project is distributed in the hope that it will be useful, but  ;;
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ;;
;; or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ;;
;; for more details.                                                          ;;
;;                                                                            ;;
;; You should have received a copy of the GNU General Public License along    ;;
;; with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[bits 16]

%include "multiboot.inc"

SECTION .header
GLOBAL HEADER
HEADER:
			db "ZBEF"
entry:		dd start
bootDisk:	db	0x00
part:
	lba:	dd 0x00000000
	size:	dd 0x00000000
	type:	db 0x00

multiboot:	dd 0


SECTION .boot

%include "gdt.inc"

GLOBAL start
start:
    cli
    xor ax, ax
    mov	ds, ax
    mov	es, ax
	mov	ss, ax
    sti

    mov ebp, enteringStage15
    xor ecx, ecx
    mov cl, byte [ebp]
    add ebp, 4
    mov ah, 0x13
    mov al, 0x01
    mov bl, 0X4F
    mov dh, 0x00
    mov dl, 0x00
    int 0x10

	mov esp, 0x8000
	mov ebp, 0x8000

	call malloc_init

	mov eax, multibootInfo_size
	call malloc
	mov [multiboot], eax

	call getMem

    mov ebp, enteringStage15
    xor ecx, ecx
    mov cl, byte [ebp]
    add ebp, 4
    mov ah, 0x13
    mov al, 0x01
    mov bl, 0X4F
    mov dh, 0x01
    mov dl, 0x00
    int 0x10

    mov ax, 0x1003
    xor bx, bx
    int 0x10

;	call setBootDev

;	call getMMap

;	call getConfig
;	call setBootloaderName
;	jmp $


;	call getAPM

    mov ebp, enteringStage15
    xor ecx, ecx
    mov cl, byte [ebp]
    add ebp, 4
    mov ah, 0x13
    mov al, 0x01
    mov bl, 0X4F
    mov dh, 0x02
    mov dl, 0x00
    int 0x10

    xor al, al

    in al, 0x92
    or al, 0x02
    out 0x92, al


;    in al, 0x92; instead of 0x93; switch A20 gate via fast A20 port 92
;    or al, 2            ; set A20 Gate bit 1
;    and al, ~1          ; clear INIT_NOW bit
;    out 0x92, al

;    gdt_init()

	; GOTO pmode
	cli
	lgdt [gdtr]
	mov eax, cr0
	or eax, 1
	mov cr0, eax

	jmp dword 0x8:stage2_setup

%include "multiboot.inc"

%define multInfo(x) x+multibootInfo
%define multInfoApm(x) x+apm
%define multInfoMMap(x) x+8+mMap


%include "malloc.inc"
%include "get_memory.inc"
%include "misc.inc"
%include "apm.inc"
%include "mmap.inc"


%define SET_A20_GATE_ON   call enable_a20_gate
%define SET_A20_GATE_OFF  call disable_a20_gate


proc enable_a20_gate
    xor al, al

    in al, 0x92
    or al, 0x02
    out 0x92, al

    ret

    return
endproc

proc disable_a20_gate
    xor al, al

    in al, 0x92
    and al, 0xfd
    out 0x92, al

    ret

    return
endproc


str enteringStage15, "ZIRCON BOOTLOADER STAGE 1.5"
zirconBootloaderName: db "Zircon bootloader v1.0.0", 0x00


halt:
	hlt
	jmp halt



;;;;;;;;;;
;; GDTR ;;
;;;;;;;;;;
gdt:
    .ns: dd 0x00, 0x00
    .cs: dw 0xFFFF, 0x0000
         db 0x00, 10011010b, 11001111b, 0x00
    .ds: dw 0xFFFF, 0x0000
         db 0x00, 10010010b, 11001111b, 0x00
gdt_end:

gdtr:
    dw gdt_end - gdt - 1
    dd gdt

SECTION .text

[bits 32]

%include "ata.inc"

func puts, ptr, attr, x, y
func putcn, char, attr, count, x, y
func putan, attr, count, x, y
func fill_rect, x, y, w, h, char, attr
func fill_rect_attr, x, y, w, h, attr

IMPORT ata_read

proc puts
    %$ptr    arg
    %$attr   arg
    %$x      arg
    %$y      arg

    mov esi, [%$ptr]
    mov edi, 0x000b8000

    mov eax, [%$y]
    mov ebx, 160
    mul ebx
    add edi, eax

    mov eax, [%$x]
    shl eax, 1
    add edi, eax

    xor eax, eax
    mov ah, [%$attr]

    .write_char:
        lodsb
        cmp al, 0x00
        je .write_end
        stosw
        jmp .write_char

    .write_end:

    return
endproc

proc putcn
    %$char   arg
    %$attr   arg
    %$count  arg
    %$x      arg
    %$y      arg

    mov edi, 0x000b8000

    mov eax, [%$y]
    mov ebx, 160
    mul ebx
    add edi, eax

    mov eax, [%$x]
    shl eax, 1
    add edi, eax

    xor eax, eax
    mov ah, [%$attr]
    mov al, [%$char]

    mov ecx, [%$count]

    .write_char:
        stosw
        loop .write_char

    return
endproc

proc putan
    %$attr   arg
    %$count  arg
    %$x      arg
    %$y      arg

    mov edi, 0x000b8000

    mov eax, [%$y]
    mov ebx, 160
    mul ebx
    add edi, eax

    mov eax, [%$x]
    shl eax, 1
    add edi, eax
    mov esi, edi

    xor eax, eax
    mov ah, [%$attr]

    mov ecx, [%$count]

    .write_char:
        lodsb
        inc esi
        stosw
        loop .write_char

    return
endproc

HEX_VALUES: db '0123456789ABCDEF'

proc puth
    %$integer arg
    %$attr    arg
    %$x       arg
    %$y       arg

    mov edx, [%$integer]
    mov ecx, 7

    LABEL .write
        mov edi, [%$x]
        add edi, ecx

        mov eax, edx
        and eax, 0x0000000F

        xor ebx, ebx
        mov bl, [eax + HEX_VALUES]

        ror edx, 4

        pushad
        putcn(DWORD [eax + HEX_VALUES], DWORD [%$attr], DWORD 1, DWORD edi, DWORD [%$y])
        popad

        loop .write

    return
endproc

proc fill_rect
    %$x      arg
    %$y      arg
    %$w      arg
    %$h      arg
    %$char   arg
    %$attr   arg

    mov ecx, [%$h]
    .loop.y:
        mov ebx, ecx
        add ebx, [%$y]
        push ecx
        putcn(DWORD [%$char], DWORD [%$attr], DWORD [%$w], DWORD [%$x], ebx)
        pop ecx
        loop .loop.y

    return
endproc

proc fill_rect_attr
    %$x      arg
    %$y      arg
    %$w      arg
    %$h      arg
    %$attr   arg

    mov ecx, [%$h]
    .loop.y:
        mov ebx, ecx
        add ebx, [%$y]
        push ecx
        putan(DWORD [%$attr], DWORD [%$w], DWORD [%$x], ebx)
        pop ecx
        loop .loop.y

    return
endproc

func puth, integer, attr, x, y

global stage2_setup
stage2_setup:
	mov ax, 0x10
	mov ds, ax
	mov ss, ax
	mov fs, ax
	mov gs, ax
	mov es, ax
	mov esp, 0x90000

	mov [0xb8000], dword 0x27691748

	mov eax, 0x2badb002
	mov ebx, multInfo()

    putcn(' ', 0x70, 80*25, 0, 0)

    puts(STAGE_2_BOOTLOADER_TITLE   + 4, 0x78, 0, 2)
    puts(STAGE_2_BOOTLOADER_VERSION + 4, 0x78, 0, 3)

    fill_rect(0, 6, 80, 17, ' ', 0x07)
    fill_rect(2, 7, 76, 15, ' ', 0x07)
    fill_rect_attr(0, 9, 80, 1, 0x17)

    puth(DWORD 0x12345678, DWORD 0x07, DWORD 72, DWORD 8)

    DO
        ata_read(1, 64, 0x00000000)
    WHILE (al EQ al)

    xor ecx, ecx

;    .loop:
;        putcn(' ', 0x00, ecx, 0, 24)
;        inc ecx
;        mov eax, ecx
;        xor edx, edx
;        mov ebx, 80
;        idiv ebx
;        mov ecx, edx
;        pause
;        jmp .loop




;    mov esi, STAGE_2_BOOTLOADER_TITLE + 4
;    mov edi, 0x000b8000
;    mov ah, 0x70
;    .write_char:
;        lodsb
;        cmp al, 0x00
;        je .write_end
;        stosw
;        jmp .write_char
;
;    .write_end:


;	call 0x00008800

stage2_halt:
	hlt
	jmp stage2_halt


SECTION .text
GLOBAL stage2
stage2:
	hlt
	jmp halt

str STAGE_2_BOOTLOADER_TITLE,   "                       ZIRCON KERNEL - STAGE 2 BOOTLOADER                       "
str STAGE_2_BOOTLOADER_VERSION, "                                            version 1.0.0                       "