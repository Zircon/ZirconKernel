%include "multiboot.inc"

global start

func kmain, key, mbi

start:
proc __START__
    mov [0x000b8000], word 0xF055
	kmain(eax, ebx)

endproc

str STAGE_2_TITLE, "Zircon Bootloader STAGE 2"