////////////////////////////////////////////////////////////////////////////////
// Copyright ? 2011-2012 Maxime Jeanson for the Zircon Project.               //
// This file is part of the Zircon Project.                                   //
//                                                                            //
// The Zircon Project is free software: you can redistribute it and/or modify //
// it under the terms of the GNU General Public License as published by the   //
// Free Software Foundation, either version 3 of the License, or              //
// (at your option) any later version.                                        //
//                                                                            //
// The Zircon Project is distributed in the hope that it will be useful, but  //
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY //
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    //
// for more details.                                                          //
//                                                                            //
// You should have received a copy of the GNU General Public License along    //
// with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       //
////////////////////////////////////////////////////////////////////////////////

#ifndef ATA_H
# define ATA_H

# include "stddef.h"

class ata
{
	public:
		ata();

		int read(unsigned char drive, unsigned int lba, unsigned short count, uint16 *buffer);

	private:
		int common(uint8 drive, uint32 lba, uint16 count);

		enum status
		{
			error = 0x01,
			index = 0x02,
			corr  = 0x04,
			drq   = 0x08,
			dsc   = 0x10,
			dwf   = 0x20,
			drdy  = 0x40,
			busy  = 0x80
		};
		enum error
		{
			amnf  = 0x01,
			tk0nf = 0x02,
			mcr   = 0x04,
			abrt  = 0x08,
			idnf  = 0x10,
			mc    = 0x20,
			unc   = 0x40,
			bbk   = 0x80
		};
		enum command
		{
			nop							= 0x00,
			cfa_request_extended_error	= 0x03,
			device_reset				= 0x08,
			read_sectors				= 0x20,
			read_sectors_ext			= 0x24,
			identify_device				= 0xec
		};
};

#endif /* ATA_H */
