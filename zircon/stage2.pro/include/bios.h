////////////////////////////////////////////////////////////////////////////////
// Copyright © 2011-2012 Maxime Jeanson for the Zircon Project.               //
// This file is part of the Zircon Project.                                   //
//                                                                            //
// The Zircon Project is free software: you can redistribute it and/or modify //
// it under the terms of the GNU General Public License as published by the   //
// Free Software Foundation, either version 3 of the License, or              //
// (at your option) any later version.                                        //
//                                                                            //
// The Zircon Project is distributed in the hope that it will be useful, but  //
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY //
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    //
// for more details.                                                          //
//                                                                            //
// You should have received a copy of the GNU General Public License along    //
// with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       //
////////////////////////////////////////////////////////////////////////////////

#ifndef BIOS_H
#define BIOS_H

void* palloc();
void  pfree(void*);
void  readDisk(uint8 disk, uint32 lba, uint16 count, void* buffer);
void  writeDisk(uint8 disk, uint32 lba, uint16 count, void* buffer);

#endif // BIOS_H
