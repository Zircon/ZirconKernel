;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright © 2011-2012 Maxime Jeanson for the Zircon Project.               ;;
;; This file is part of the Zircon Project.                                   ;;
;;                                                                            ;;
;; The Zircon Project is free software: you can redistribute it and/or modify ;;
;; it under the terms of the GNU General Public License as published by the   ;;
;; Free Software Foundation, either version 3 of the License, or              ;;
;; (at your option) any later version.                                        ;;
;;                                                                            ;;
;; The Zircon Project is distributed in the hope that it will be useful, but  ;;
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY ;;
;; or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    ;;
;; for more details.                                                          ;;
;;                                                                            ;;
;; You should have received a copy of the GNU General Public License along    ;;
;; with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

%define MEM_FREE	0xfffffffe
%define MEM_USED	1

[bits 16]

extern bootEnd

struc BDA
	.IOCom1:	resb 2
	.IOCom2:	resb 2
	.IOCom3:	resb 2
	.IOCom4:	resb 2
	.IOLpt1:	resb 2
	.IOLpt2:	resb 2
	.IOLpt3:	resb 2
	.IOLpt4:	resb 2
	.equipment:	resb 2
	.if:		resb 1
	.memSize:	resb 2
	.memError:	resb 2
	.kbrdFlag1:	resb 1
	.kbrdFlag2:	resb 1
	.altNum:	resb 1
	.kbrdNext:	resb 2
	.kbrdPrev:	resb 2
	.kbrdBuff:	resb 32
	.fddCalStat:	resb 1
	.fddMotStat:	resb 1
	.fddMotTimeOut:	resb 1
	.fddStat:		resb 1
	.hdFdcStat0:	resb 1
	.hdFdcStat1:	resb 1
	.hdFdcStat2:	resb 1
endstruc

func palloc
func pfree, ptr
func pmmSet, ptr
func pmmSets, ptr, size
func pmmInit

func initMultiboot
func getMmap
func getMem

