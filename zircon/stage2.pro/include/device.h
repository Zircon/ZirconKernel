////////////////////////////////////////////////////////////////////////////////
// Copyright © 2011-2012 Maxime Jeanson for the Zircon Project.               //
// This file is part of the Zircon Project.                                   //
//                                                                            //
// The Zircon Project is free software: you can redistribute it and/or modify //
// it under the terms of the GNU General Public License as published by the   //
// Free Software Foundation, either version 3 of the License, or              //
// (at your option) any later version.                                        //
//                                                                            //
// The Zircon Project is distributed in the hope that it will be useful, but  //
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY //
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    //
// for more details.                                                          //
//                                                                            //
// You should have received a copy of the GNU General Public License along    //
// with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       //
////////////////////////////////////////////////////////////////////////////////

#ifndef DEVICE_H
# define DEVICE_H

//==============================================================================
// For more simplicity, we should manage that in C++ class organized. This will
// permit to have specific extended child for instance, an ata driver want to 
// extend the driverPST_HDD class to one considering it's own function and own
// extended one will be able to do so. But, for partability purpose, the C
// implemented structures should be allowed in the devKit. It's a possibility...
//==============================================================================

// The system have to implement the basic management code to allow device driver
// to register their devices when probed.

struct deviceList
{
	device*	devParent;
	device*	first;
	device*	last;
};

struct driverPST
{
	driverType	type;
	union
	{
		driverPST_HDD disk;
	}
};

struct driverPST_HDD
{
	void (*diskInitialize)(device*)										init;
	void (*diskReset)(device*)											reset;
	void (*diskRead)(device*, uint32, uint16, void*)					read;
	void (*diskWrite)(device*, uint32, uint16, void*)					write;
	void (*diskEject)(device*)											eject;
	uint32	type
	union
	{
		// TODO: add the specific function for ATA / IDE / SCSI / etc in
		//		 structures named driverPST_HDD_DEVTYPE
	}
};

struct bus
{
	// The ID field is initialized at registration time by the system and send
	// to the driver who have to store it somewhere. This is the only way to be 
	// sure, for the driver, that's the right device.
	uint32		ID;
	uint16		type;
	uint16		class;
	driverPST*	pst;
	bus*		parent;
	deviceList*	child;
	device*		prev;
	device*		next;
	uint32		deviceDataSize;
	uint8*		devData;
} root;

struct device
{
	// The ID feild is initialized at registration time by the system and send
	// to the driver who have to store it somewhere. This is the only way to be 
	// sure, for the driver, that's the right device.
	uint32		ID;
	uint16		type;
	uint16		class;
	driverPST*	pst;
	bus*		parent;
	deviceList*	child;
	device*		prev;
	device*		next;
	uint32		deviceDataSize;
	uint8*		devData;
};

extern uint32 registerDevice(void* driver, device* dev);

#endif /* DEVICE_H */
