////////////////////////////////////////////////////////////////////////////////
// Copyright � 2011-2012 Maxime Jeanson for the Zircon Project.               //
// This file is part of the Zircon Project.                                   //
//                                                                            //
// The Zircon Project is free software: you can redistribute it and/or modify //
// it under the terms of the GNU General Public License as published by the   //
// Free Software Foundation, either version 3 of the License, or              //
// (at your option) any later version.                                        //
//                                                                            //
// The Zircon Project is distributed in the hope that it will be useful, but  //
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY //
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    //
// for more details.                                                          //
//                                                                            //
// You should have received a copy of the GNU General Public License along    //
// with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       //
////////////////////////////////////////////////////////////////////////////////

#ifndef FAT_H
# define FAT_H

    typedef unsigned char		uint8;
    typedef unsigned short		uint16;
    typedef unsigned int		uint32;
    typedef unsigned long long	uint64;

    typedef signed char		sint8;
    typedef signed short	sint16;
    typedef signed int		sint32;

    typedef struct	FAT12_BPB	FAT12_BPB;
    typedef struct	FAT12_BPB	FAT16_BPB;
    typedef struct	FAT32_BPB	FAT32_BPB;
    typedef struct	FAT_BPB		FAT_BPB;
    typedef union	fat_dirEntry	fat_dirEntry;
    typedef struct	longDirEntry longDirEntry;
    typedef struct	shortDirEntry shortDirEntry;


    void* fatOpen(char* fileName);
    void fatInit(void* fatImg);

    struct FAT32_BPB
    {
        uint32	BPB_FATSz32;
        uint16	BPB_ExtFlags;
        uint16	BPB_FSVer;
        uint32	BPB_RootClus;
        uint16	BPB_FSInfo;
        uint16	BPB_BkBootSec;
        uint8	UNUSED0[12];
        uint8	BS_DrvNum;
        uint8	UNUSED1;
        uint8	BS_BootSig;
        uint32	BS_VolID;
        uint8	BS_VolLab[11];
        uint8	BS_FilSysType[8];
    };

    struct FAT12_BPB
    {
        uint8	BS_DrvNum;
        uint8	UNUSED0;
        uint8	BS_BootSig;
        uint32	BS_VolID;
        uint8	BS_VolLab[11];
        uint8	BS_FilSysType[8];
    };


    struct __attribute__((packed)) FAT_BPB
    {
        uint8	BS_jmpBoot[3];
        uint8	BS_OEMName[8];
        uint16	BPB_BytePerSec;
        uint8	BPB_SecPerClus;
        uint16	BPB_RsvdSecCnt;
        uint8	BPB_NumFATs;
        uint16	BPB_RootEntCnt;
        uint16	BPB_TotSec16;
        uint8	BPB_Media;
        uint16	BPB_FATSz16;
        uint16	BPB_SecPerTrk;
        uint16	BPB_NumHeads;
        uint32	BPB_HiddSec;
        uint32	BPB_TotSec32;

        union
        {
            FAT12_BPB	fat12;
            FAT16_BPB	fat16;
            FAT32_BPB	fat32;
        };
    } *fat_bpb;

    struct shortDirEntry
    {
        uint8	name[8];
        uint8	ext[3];
        union
        {
            uint8	attr;
            struct
            {
                uint8	ro			:1;
                uint8	hidden		:1;
                uint8	system		:1;
                uint8	volume_id	:1;
                uint8	directory	:1;
                uint8	archive		:1;
            };
        };
        uint8	ntres;
        uint8	creationTimeTenth;
        uint16	firstClusterHI;
        uint16	writeTime;
        uint16	writeDate;
        uint16	firstClusterLO;
        uint32	fileSize;
    };

    struct longDirEntry
    {
        uint8	order;
        uint8	name1[10];
        uint8	attr;
        uint8	type;
        uint8	checksum;
        uint8	name2[12];
        uint16	UNUSED0;
        uint8	name3[2];
    };

    union __attribute__((packed)) fat_dirEntry
    {
        uint8			order;
        shortDirEntry	shortDir;
        longDirEntry	longDir;
    };



#endif /* FAT_H */
