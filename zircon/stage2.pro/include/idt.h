////////////////////////////////////////////////////////////////////////////////
// Copyright © 2011 Maxime Jeanson for the Zircon Project.                    //
// This file is part of the Zircon Project.                                   //
//                                                                            //
// The Zircon Project is free software: you can redistribute it and/or modify //
// it under the terms of the GNU General Public License as published by the   //
// Free Software Foundation, either version 3 of the License, or              //
// (at your option) any later version.                                        //
//                                                                            //
// The Zircon Project is distributed in the hope that it will be useful, but  //
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY //
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    //
// for more details.                                                          //
//                                                                            //
// You should have received a copy of the GNU General Public License along    //
// with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       //
////////////////////////////////////////////////////////////////////////////////


#ifndef IDT_H
#define IDT_H

#include "stddef.h"

class idt
{
	public:
		idt();
		void init();
		void add(void(*isrHandler)(void), uint16 entry);
		void load();

	private:
		struct idtEntry
		{
			uint16	offset0;
			uint16	sel;
			uint16	type;
			uint16	offset1;
		}__attribute__ ((packed));
		struct idtRegister
		{
			uint16		size;
			idtEntry	*idt;
		}__attribute__ ((packed));

		idtRegister idtr;
};

#endif // IDT_H
