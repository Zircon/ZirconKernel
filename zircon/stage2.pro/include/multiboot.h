////////////////////////////////////////////////////////////////////////////////
// Copyright © 2011 Maxime Jeanson for the Zircon Project.                    //
// This file is part of the Zircon Project.                                   //
//                                                                            //
// The Zircon Project is free software: you can redistribute it and/or modify //
// it under the terms of the GNU General Public License as published by the   //
// Free Software Foundation, either version 3 of the License, or              //
// (at your option) any later version.                                        //
//                                                                            //
// The Zircon Project is distributed in the hope that it will be useful, but  //
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY //
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    //
// for more details.                                                          //
//                                                                            //
// You should have received a copy of the GNU General Public License along    //
// with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       //
////////////////////////////////////////////////////////////////////////////////


#ifndef MULTIBOOT_H
#define MULTIBOOT_H

struct multiboot1
{
		uint32	memFlag			: 1;
		uint32	bootDevice		: 1;
		uint32	cmdLine			: 1;
		uint32	mods			: 1;
		uint32	syms			: 2;
		uint32	mmap			: 1;
		uint32	drives			: 1;
		uint32	configTable		: 1;
		uint32	bootLoaderName	: 1;
		uint32	apmTable		: 1;
		uint32	vbe				: 1;

		uint32	mem_lower;
		uint32	mem_upper;

		uint32	boot_device;

		uint32	cmdline;

		uint32	mods_count;
		uint32	mods_addr;

		struct	syms
		{
				uint32	num,
						size,
						addr,
						shndx;
		};

		uint32	mmap_length;
		uint32	mmap_addr;

		uint32	drives_length;
		uint32	drives_addr;

		uint32	config_table;

		uint32	boot_loader_name;

		uint32	apm_table;

		uint32	vbe_control_info;
		uint32	vbe_mode_info;
		uint32	vbe_mode;
		uint32	vbe_interface_seg;
		uint32	vbe_interface_off;
		uint32	vbe_interface_len;
};

#endif // MULTIBOOT_H
