////////////////////////////////////////////////////////////////////////////////
// Copyright ? 2011-2012 Maxime Jeanson for the Zircon Project.               //
// This file is part of the Zircon Project.                                   //
//                                                                            //
// The Zircon Project is free software: you can redistribute it and/or modify //
// it under the terms of the GNU General Public License as published by the   //
// Free Software Foundation, either version 3 of the License, or              //
// (at your option) any later version.                                        //
//                                                                            //
// The Zircon Project is distributed in the hope that it will be useful, but  //
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY //
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License    //
// for more details.                                                          //
//                                                                            //
// You should have received a copy of the GNU General Public License along    //
// with the Zircon Project. If not, see <http://www.gnu.org/licenses/>.       //
////////////////////////////////////////////////////////////////////////////////

#ifndef STDDEF_H
#define STDDEF_H

/* We define the null pointer for both C and C++ */
# ifdef __cplusplus
#  define NULL	0			// C++
# else
#  define NULL	(void*)0	// C
# endif /* __cplusplus */

/* This header is for system integration */
# include "sys.h"
# include "cdefs.h"

/* For Zircon only, we define the bool type */
# ifdef __ZIRCON__
#  ifndef __cplusplus
	typedef enum bool bool;

	enum bool
		{ true = -1, false = 0 };
#  endif /* __cplusplus */
	/* We define types for Zircon */

	/* Unsigned */
	typedef unsigned char  uint8;
	typedef unsigned short uint16;
	typedef unsigned int   uint32;

	/* Signed */
	typedef signed char  int8;
	typedef signed short int16;
	typedef signed int   int32;
# endif /* __ZIRCON__ */

#endif // STDDEF_H
